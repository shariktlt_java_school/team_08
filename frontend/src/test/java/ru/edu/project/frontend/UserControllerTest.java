package ru.edu.project.frontend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserForm;
import ru.edu.project.backend.api.students.UserService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
    @Mock
    private Model modelMock;
    @Mock
    private UserService userService;
    @Mock
    private PasswordEncoder encoderMock;
    @InjectMocks
    private UserController controller;

    @Test
    public void index() {
        ArrayList<User> exceptedUserList = new ArrayList<>();
        when(userService.getUsers()).thenReturn(exceptedUserList);

        String viewName = controller.index(modelMock);
        assertEquals("/user/index", viewName);
        verify(modelMock).addAttribute("users", exceptedUserList);
    }

    @Test
    public void createForm() {
        String viewName = controller.createForm(modelMock);
        assertEquals("/user/create",viewName );
        verify(modelMock).addAttribute("roles", User.UserRole.values());
    }

    @Test
    public void createFormProcessing() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(false);

        when(encoderMock.encode(anyString())).thenReturn("111");

        User expectedUser = User.builder().id(111L).build();
        UserController.CreateForm form = new UserController.CreateForm();
        form.setName("name");
        form.setSurname("surname");
        form.setRole(User.UserRole.Student);
        form.setPassword("111");
        form.setLogin("login");
        form.setMiddleName("middle");
        when(userService.createUser(any(UserForm.class))).thenAnswer(invocationOnMock -> {
           UserForm userForm = invocationOnMock.getArgument(0, UserForm.class);
           assertEquals(form.getName(), userForm.getName());
           assertEquals(form.getSurname(), userForm.getSurname());
           assertEquals(form.getRole(), userForm.getRole());
           assertEquals(form.getPassword(), userForm.getPassword());
           assertEquals(form.getMiddleName(), userForm.getMiddleName());
           assertEquals(form.getLogin(), userForm.getLogin());
           return expectedUser;
        });
        String viewName = controller.createFormProcessing(form, bindingResultMock, modelMock);
        assertEquals("redirect:/user/?created=" + expectedUser.getId(), viewName);

        verify(modelMock, times(0)).addAttribute(anyString(), any());
        verify(userService).createUser(any(UserForm.class));
    }

    @Test
    public void createFormProcessingHasErrors() {
        List<ObjectError> mockErrors = new ArrayList<>();
        BindingResult bindingResultMock = mock(BindingResult.class);
        when(bindingResultMock.hasErrors()).thenReturn(true);
        when(bindingResultMock.getAllErrors()).thenReturn(mockErrors);

        String viewName = controller.createFormProcessing(
                new UserController.CreateForm(),
                bindingResultMock,
                modelMock);
        assertEquals("/user/create", viewName);
        verify(modelMock).addAttribute("roles", User.UserRole.values());
        verify(modelMock).addAttribute("errors", mockErrors);
    }
}