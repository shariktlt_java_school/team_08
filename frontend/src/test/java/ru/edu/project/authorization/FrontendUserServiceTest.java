package ru.edu.project.authorization;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class FrontendUserServiceTest {
    @Mock
    private UserService userServiceMock;

    @InjectMocks
    private FrontendUserService frontendUserService;

    @Test
    public void loadUserByUsername() {
        User user = User.builder().id(111L).role(User.UserRole.Student).login("admin").password("111").build();
        when(userServiceMock.getUserByLogin(anyString())).thenReturn(user);
        UserDetails details = frontendUserService.loadUserByUsername("admin");
        assertEquals(user.getId(), details.getId());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadNotExistsUser() {
        when(userServiceMock.getUserByLogin(anyString())).thenThrow(IllegalArgumentException.class);
        frontendUserService.loadUserByUsername("admin");
    }
}