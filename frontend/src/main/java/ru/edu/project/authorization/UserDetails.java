package ru.edu.project.authorization;

import lombok.Getter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

@Getter
public class UserDetails extends User {
    /**
     * user id.
     */
    private Long id;

    /**
     * @param id1
     * @param username
     * @param password
     * @param role
     */
    public UserDetails(final Long id1, final String username, final String password, final String role) {
        super(username, password, AuthorityUtils.createAuthorityList(role));
        id = id1;
    }
}
