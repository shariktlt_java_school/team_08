package ru.edu.project.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserService;

@Service
public class FrontendUserService implements UserDetailsService {
    /**
     * Зависимость.
     */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Зависимость на бэкенд сервис.
     */
    @Autowired
    private UserService userService;

    /**
     * Реализация метода поиска информации о клиенте.
     *
     * @param username
     * @return userDetails
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user;
        try {
            user = userService.getUserByLogin(username);
        } catch (IllegalArgumentException exc) {
            throw new UsernameNotFoundException("user not found");
        }
        return new UserDetails(
                user.getId(),
                user.getLogin(),
                user.getPassword(),
                user.getRole().name()
        );
    }
}
