package ru.edu.project.frontend;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.project.authorization.UserDetails;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseForm;
import ru.edu.project.backend.api.courses.CourseInfo;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentService;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RequestMapping("/course")
@Controller
public class CoursesController {

    /**
     * Логгер.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CoursesController.class);

    /**
     * Служба получения курсов.
     */
    @Autowired
    private CourseService courseService;

    /**
     * Служба получения пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Служба получения групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Служба получения таблицы, связывающей пользователя и группу.
     */
    @Autowired
    private GroupStudentService groupStudentService;

    /**
     * Служба получения уроков.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * Получить страницу со списком курсов.
     *
     * @param model
     * @param authentication
     * @return String
     */
    @GetMapping("/")
    public String index(final Model model, final Authentication authentication) {
        List<String> rolesList = getRolesList(authentication);
        model.addAttribute("roles", rolesList);
        if (rolesList.contains(User.UserRole.Student.name())) {
            model.addAttribute(
                    "courses",
                    getCoursesInfoForUser(((UserDetails) authentication.getPrincipal()).getId()));
            model.addAttribute("user",
                    userService.getUserByLogin(((UserDetails) authentication.getPrincipal()).getUsername()));
            return "/course/courses_for_student";
        } else if (rolesList.contains(User.UserRole.Teacher.name())) {
            model.addAttribute("courses",
                    getCoursesInfoForTeacher(((UserDetails) authentication.getPrincipal()).getId()));
            return "/course/index";
        } else {
            model.addAttribute("courses", getCoursesInfo());
            return "/course/index";
        }
    }

    /**
     * Форма просмотра курса.
     *
     * @param model
     * @param id
     * @param authentication
     * @return String
     */
    @GetMapping("/{id}")
    public String getLightView(final Model model, @PathVariable final Long id, final Authentication authentication) {
        CourseInfo courseInfo = getCourseInfo(id);
        if (authentication != null && getRolesList(authentication).contains(User.UserRole.Student.name())) {
            // для студентов
            List<Long> usersCourses = getCoursesInfoForUser(((UserDetails) authentication.getPrincipal()).getId())
                    .stream()
                    .map(c -> c.getCourse().getId())
                    .collect(Collectors.toList());
            if (!usersCourses.contains(id)) {
                model.addAttribute("error", "доступ запрещен");
                return "/common/error_page";
            } else {
                List<Lesson> filteredLessons = lessonService
                        .getLessons()
                        .stream()
                        .filter(lesson -> id == lesson.getCourseId())
                        .collect(Collectors.toList());
                model.addAttribute("course", courseInfo);
                return "/course/course_for_student";
            }
        } else { // для всех пользователей, в т. ч. анонимных
            courseInfo.setGroups(null);
            model.addAttribute("course", courseInfo);
            return "/course/lightview";
        }
    }

    /**
     * Получить страницу с формой создания нового курса.
     *
     * @param model
     * @param authentication
     * @return String
     */
    @GetMapping("/create")
    public String createForm(final Model model, final Authentication authentication) {
        if (getRolesList(authentication).contains(User.UserRole.Administrator.name())) {
            model.addAttribute(
                    "teachers",
                    userService
                            .getUsers()
                            .stream()
                            .filter(user -> user.getRole().equals(User.UserRole.Teacher))
                            .collect(Collectors.toList()));
            return "/course/create";
        } else {
            model.addAttribute("error", "доступ запрещен");
            return "/common/error_page";
        }
    }

    /**
     * Создание курса.
     *
     * @param form
     * @param bindingResult
     * @param model
     * @param authentication
     * @return String
     */
    @PostMapping("/create")
    public String createFormProcessing(@Valid @ModelAttribute final CreateForm form,
                                       final BindingResult bindingResult,
                                       final Model model,
                                       final Authentication authentication) {
        if (getRolesList(authentication).contains(User.UserRole.Administrator.name())) {
            LOG.info("Method started: createFormProcessing");
            if (bindingResult.hasErrors()) {
                model.addAttribute("errors", bindingResult.getAllErrors());
                return createForm(model, authentication);
            }
            if (form.getTeacherId() < 0) {
                form.setTeacherId(null);
            }
            if (!StringUtils.isBlank(form.getTitle())) {
                LOG.info("Before course creation: {}", form.getTitle());
                Course createdCourse = courseService.createCourse(
                        CourseForm
                                .builder()
                                .title(form.getTitle())
                                .description(form.getDescription())
                                .teacherId(form.getTeacherId())
                                .build());
                LOG.info("Created course: {}", createdCourse);
                return "redirect:/course/?created=" + createdCourse.getId();
            } else {
                return "redirect:/course/";
            }
        } else {
            model.addAttribute("error", "доступ запрещен");
            return "/common/error_page";
        }
    }

    /**
     * Получить страницу с формой редактирования курса.
     *
     * @param model
     * @param id
     * @param authentication
     * @return String.
     */
    @GetMapping("/edit")
    public String editForm(final Model model, @RequestParam final Long id, final Authentication authentication) {
        List<String> rolesList = getRolesList(authentication);
        model.addAttribute("roles", rolesList);
        if (rolesList.contains(User.UserRole.Administrator.name())
                || (rolesList.contains(User.UserRole.Teacher.name())
                && courseService.getCourse(id).getTeacherId()
                == ((UserDetails) authentication.getPrincipal()).getId())) {
            model.addAttribute(
                    "course",
                    getCourseInfo(id));
            model.addAttribute(
                    "teachers",
                    userService
                            .getUsers()
                            .stream()
                            .filter(user -> user.getRole().equals(User.UserRole.Teacher))
                            .collect(Collectors.toList()));
            model.addAttribute(
                    "groups",
                    groupService.getByCourse(id));
            return "/course/edit";
        } else {
            model.addAttribute("error", "Доступ запрещен");
            return "/common/error_page";
        }
    }

    /**
     * Сохранение отредактированного курса.
     *
     * @param form
     * @param bindingResult
     * @param model
     * @param authentication
     * @return String
     */
    @PostMapping("/edit")
    public String saveChangedCourse(@Valid @ModelAttribute final CreateForm form,
                                    final BindingResult bindingResult,
                                    final Model model,
                                    final Authentication authentication) {
        List<String> rolesList = getRolesList(authentication);
        if (rolesList.contains(User.UserRole.Administrator.name())
                || (rolesList.contains(User.UserRole.Teacher.name())
                && courseService.getCourse(form.getId()).getTeacherId()
                == ((UserDetails) authentication.getPrincipal()).getId())) {
            LOG.info("Method started: saveChangedCourse");
            if (bindingResult.hasErrors()) {
                model.addAttribute("errors", bindingResult.getAllErrors());
                return createForm(model, authentication);
            }
            if (form.getId() != null) {
                LOG.info("Course should be changed: {}", form);
                Course changedCourse = courseService.saveCourse(
                        CourseForm
                                .builder()
                                .id(form.getId())
                                .title(form.getTitle())
                                .description(form.getDescription())
                                .teacherId(form.getTeacherId() < 0 ? null : form.getTeacherId())
                                .build());
                LOG.info("Changed course: {}", changedCourse);
                return "redirect:/course/?edited=" + changedCourse.getId();
            } else {
                return "redirect:/course/";
            }
        }
        model.addAttribute("error", "Доступ запрещен");
        return "/common/error_page";
    }

    /**
     * Вернем список ролей для объекта authentication.
     *
     * @param authentication
     * @return List<String>
     */
    private List<String> getRolesList(final Authentication authentication) {
        return authentication
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
    }

    /**
     * Вернем список курсов для учителя.
     *
     * @param teacherId
     * @return List<CourseInfo>
     */
    private List<CourseInfo> getCoursesInfoForTeacher(final Long teacherId) {
        List<Course> filteredCourses = courseService.getCourses()
                .stream()
                .filter(course -> course.getTeacherId() != null && course.getTeacherId() == teacherId)
                .collect(Collectors.toList());
        List<CourseInfo> result = new ArrayList<>();
        for (int i = 0; i < filteredCourses.size(); i++) {
            result.add(getCourseInfo(filteredCourses.get(i)));
        }
        return result;
    }

    /**
     * Вернем список курсов для пользователя.
     *
     * @param userId
     * @return List<CourseInfo>
     */
    private List<CourseInfo> getCoursesInfoForUser(final Long userId) {
        List<Group> groups = groupService.getGroups();
        List<GroupStudent> groupStudents = groupStudentService.getGroupStudents();
        List<Long> linkedGroups = groupStudents
                .stream()
                .filter(record -> Objects.equals(record.getUserId(), userId))
                .map(GroupStudent::getGroupId)
                .collect(Collectors.toList());
        List<Group> groupsForUsers = groups
                .stream()
                .filter(group -> linkedGroups.contains(group.getId()))
                .collect(Collectors.toList());
        List<CourseInfo> result = new ArrayList<>();
        for (int i = 0; i < groupsForUsers.size(); i++) {
            result.add(getCourseInfo(groupsForUsers.get(i).getCourseId()));

        }
        return result;
    }

    /**
     * CreateForm class.
     */
    @Getter
    @Setter
    @ToString
    @Builder
    public static class CreateForm {

        /**
         * id курса.
         */
        private Long id;

        /**
         * /**
         * Наименование курса.
         */
        @NotNull
        @NotEmpty
        private String title;

        /**
         * Наименование курса.
         */
        @NotNull
        @NotEmpty
        private String description;

        /**
         * Id препоадвателя.
         */
        private Long teacherId;
    }

    /**
     * Вернем информацию о курсе по id.
     *
     * @param id - id курса
     * @return CourseInfo
     */
    private CourseInfo getCourseInfo(final Long id) {
        Course course = courseService.getCourse(id);
        return getCourseInfo(course);
    }

    /**
     * Вернем информацию о курсе.
     *
     * @param course
     * @return CourseInfo
     */
    private CourseInfo getCourseInfo(final Course course) {
        User linkedTeacher;
        if (course.getTeacherId() == null) {
            linkedTeacher = null;
        } else {
            linkedTeacher = userService
                    .getUsers()
                    .stream()
                    .filter(user -> user.getId().equals(course.getTeacherId()))
                    .findFirst()
                    .orElse(null);
        }
        List<Group> linkedGroups = groupService.getByCourse(course.getId());
        List<Lesson> lessons = lessonService
                .getLessons()
                .stream()
                .filter(lesson -> lesson.getCourseId() == course.getId())
                .collect(Collectors.toList());

        return CourseInfo
                .builder()
                .course(course)
                .teacher(linkedTeacher)
                .groups(linkedGroups)
                .lessons(lessons)
                .build();
    }

    /**
     * Вернем информацию о курсах.
     *
     * @return List<CourseInfo>
     */
    private List<CourseInfo> getCoursesInfo() {
        List<CourseInfo> result = new ArrayList<>();
        for (Course course : courseService.getCourses()) {
            result.add(getCourseInfo(course));
        }
        return result;
    }
}

