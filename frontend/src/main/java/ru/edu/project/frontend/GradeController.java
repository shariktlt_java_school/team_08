package ru.edu.project.frontend;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.edu.project.authorization.UserDetails;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.api.grades.GradeForm;
import ru.edu.project.backend.api.grades.GradeService;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.api.groups.GroupStudentService;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/grade")
@Controller
public class GradeController {

    /**
     * Логгер.
     */
    private static final Logger LOG = LoggerFactory.getLogger(GradeController.class);

    /**
     * Grade service.
     */
    @Autowired
    private GradeService gradeService;

    /**
     * Lesson service.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * User service.
     */
    @Autowired
    private UserService userService;

    /**
     * Group service.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Course service.
     */
    @Autowired
    private CourseService courseService;

    /**
     * Group service.
     */
    @Autowired
    private GroupStudentService groupStudentService;

    /**
     * index GET method.
     * @param model
     * @return String
     */
    @GetMapping("/")
    public String index(final Model model) {
        model.addAttribute("activeGrades", true);

        List<Group> groupsList = groupService.getGroups();
        HashMap<String, Group> groups = new HashMap<>();
        groupsList.forEach(group -> {
            groups.put(group.getId().toString(), group);
        });
        model.addAttribute("groups", groups);

        List<GroupStudent> groupsStudentsList = groupStudentService.getGroupStudents();
        HashMap<String, GroupStudent> groupStudents = new HashMap<>();
        groupsStudentsList.forEach(groupStudent -> {
            groupStudents.put(groupStudent.getId().toString(), groupStudent);
        });
        model.addAttribute("groupStudents", groupStudents);

        List<Lesson> lessonsList = lessonService.getLessons();
        HashMap<String, Lesson> lessons = new HashMap<>();
        lessonsList.forEach(lesson -> {
            lessons.put(lesson.getId().toString(), lesson);
        });
        model.addAttribute("lessons", lessons);

        List<User> studentsList = userService
                .getUsers()
                .stream()
                .filter(user -> user.getRole().equals(User.UserRole.Student))
                .collect(Collectors.toList());

        HashMap<String, User> students = new HashMap<>();
        studentsList.forEach(student -> {
            students.put(student.getId().toString(), student);
        });
        model.addAttribute("students", students);

        model.addAttribute("grades",
                gradeService.getGrades());
        return "/grade/index";
    }

    /**
     * index GET method.
     * @param model
     * @return String
     */
    @GetMapping("/all")
    public String all(final Model model) {
        model.addAttribute("activeGrades", true);
        model.addAttribute("grades",
                gradeService.getGrades());
        return "/grade/index";
    }

    /**
     * index GET method.
     * @param id
     * @return String
     */
    @GetMapping("/delete")
    public String delete(@RequestParam(defaultValue = "0") final Long id) {
        Boolean isDeleted = gradeService.deleteGrade(id);
        return "redirect:/grade/?deleted=" + isDeleted;
    }

    /**
     * Список курсов, форма выбора.
     *
     * @param model
     * @return String
     */
    @GetMapping("/courses")
    public String coursesList(final Model model) {
        model.addAttribute("activeGrades", true);
        model.addAttribute("courses",
                courseService.getCourses());
        return "/grade/courses";
    }

    /**
     * create POST method.
     * @param course
     * @param model
     * @return String
     */
    @PostMapping("/courses")
    public String coursesList(
            @Valid
            @RequestParam final String course,
            final Model model
    ) {
        model.addAttribute("activeGrades", true);
        return "redirect:/grade/lesson_groups?course=" + course;
    }

    /**
     * Форма выбора группы и урока.
     * @param course
     * @param model
     * @return String
     */
    @GetMapping("/lesson_groups")
    public String lessonGroupsList(
            @RequestParam final String course,
            final Model model) {
        model.addAttribute("activeGrades", true);
        model.addAttribute("groups",
                groupService.getGroups());
        model.addAttribute("lessons",
                lessonService.getLessons());
        return "/grade/lesson_groups";
    }

    /**
     * create POST method.
     * @param lesson
     * @param group
     * @param model
     * @return String
     */
    @PostMapping("/lesson_groups")
    public String lessonGroupsList(
            @Valid
            @RequestParam final String lesson,
            @RequestParam final String group,
            final Model model
    ) {
        model.addAttribute("activeGrades", true);
        return "redirect:/grade/students?lesson=" + lesson + "&group=" + group;
    }


    /**
     * index GET method.
     * Собирает данные для формирования списка студентов
     * для проставления оценок.
     *
     * @param group
     * @param lesson
     * @param model
     * @return String
     */
    @GetMapping("/students")
    public String students(final Model model,
                               @RequestParam final String lesson,
                               @RequestParam final String group
    ) {
        model.addAttribute("activeGrades", true);
        model.addAttribute("lesson_id", lesson);
        model.addAttribute("group_id", group);
        model.addAttribute("group", groupService.getGroup(Long.parseLong(group)));
        model.addAttribute("lesson", lessonService.getLesson(Long.parseLong(lesson)));

        List<User> users = userService
                .getUsers()
                .stream()
                .filter(user -> user.getRole().equals(User.UserRole.Student))
                .collect(Collectors.toList());
        model.addAttribute("users", users);

        HashMap<String, User> studentsUsers = new HashMap<>();
        users.forEach(studentUser -> {
            studentsUsers.put(studentUser.getId().toString(), studentUser);
        });
        model.addAttribute("studentsUsers", studentsUsers);

        List<GroupStudent> groupStudents = groupStudentService
                .getGroupStudents()
                .stream()
                .filter(student -> student.getGroupId().toString().equals(group))
                .collect(Collectors.toList());
        model.addAttribute("students", groupStudents);

        List<Grade> gradesList = gradeService
                .getGrades()
                .stream()
                .filter(grade -> grade.getLessonId().equals(lesson))
                .collect(Collectors.toList());

        HashMap<String, String> gradesMap = new HashMap<>();
        gradesList.forEach(grade -> {
            gradesMap.put(grade.getGroupStudentId(), grade.getGrade());
        });
        model.addAttribute("groupStudentGrades", gradesMap);

        HashMap<String, String> gradesMapIds = new HashMap<>();
        gradesList.forEach(grade -> {
            gradesMapIds.put(grade.getGroupStudentId(), grade.getId().toString());
        });
        model.addAttribute("groupStudentGradesIds", gradesMapIds);
        return "/grade/students";
    }

    /**
     * create GET method.
     * @param model
     * @return String.
     */
    @GetMapping("/create")
    public String createForm(final Model model) {
        model.addAttribute("activeGrades", true);
        // создает мапу для курсов
        List<Course> coursesList = courseService.getCourses();
        HashMap<String, String> allCourses = new HashMap<>();
        coursesList.forEach(course -> {
            allCourses.put(course.getId().toString(), course.getTitle());
        });
        model.addAttribute("group_students", groupStudentService.getGroupStudents());
        model.addAttribute("courses",
                allCourses);
        model.addAttribute("lessons",
                lessonService.getLessons());

        List<User> users = userService.getUsers();
        HashMap<String, User> studentsUsers = new HashMap<>();
        users.forEach(studentUser -> {
            studentsUsers.put(studentUser.getId().toString(), studentUser);
        });
        model.addAttribute("studentsUsers", studentsUsers);
        return "/grade/create";
    }

    /**
     * create POST method.
     * @param form
     * @param bindingResult
     * @param model
     * @return String
     */
    @PostMapping("/create")
    public String createFormProcessing(
            @Valid
            @ModelAttribute final GradeController.CreateForm form,
            final BindingResult bindingResult,
            final Model model
    ) {
        model.addAttribute("activeGrades", true);
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return createForm(model);
        }
        Grade newGrade = gradeService.createGrade(GradeForm.builder().
                lessonId(form.getLessonId()).
                groupStudentId(form.getGroupStudentId()).
                grade(form.getGrade()).
                build());
        return "redirect:/grade/?created=" + newGrade.getId();
    }

    /**
     * Получить страницу с формой редактирования оценки.
     *
     * @param model
     * @param id
     * @return String.
     */
    @GetMapping("/edit")
    public String editForm(final Model model, @RequestParam final Long id) {
        model.addAttribute("activeGrades", true);
        model.addAttribute(
                "lessons",
                lessonService.getLessons());
        model.addAttribute(
                "grade",
                gradeService.getGrade(id));
        return "/grade/edit";
    }

    /**
     * Сохранение отредактированной оценки.
     *
     * @param form
     * @param bindingResult
     * @param model
     * @return String
     */
    @PostMapping("/edit")
    public String saveChangedGrade(@Valid @ModelAttribute final CreateForm form,
                                    final BindingResult bindingResult,
                                    final Model model) {
        LOG.info("Method started: saveChangedGrade");
        model.addAttribute("activeGrades", true);
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return createForm(model);
        }
        if (form.getId() != null) {
            LOG.info("Grade should be changed: {}", form);
            Grade changedGrade = gradeService.saveGrade(
                    GradeForm.builder().
                            id(form.getId()).
                            lessonId(form.getLessonId()).
                            groupStudentId(form.getGroupStudentId()).
                            grade(form.getGrade()).
                            build());
            LOG.info("Changed grade: {}", changedGrade);
            return "redirect:/grade/?edited=" + changedGrade.getId();
        } else {
            return "redirect:/grade/";
        }
    }

    /**
     * Зачетка студента с оценками.
     *
     * @param authentication
     * @param model
     * @return String
     */
    @GetMapping("/student")
    public String student(final Model model, final Authentication authentication) {
        Long userId = userService
                .getUserByLogin(((UserDetails) authentication.getPrincipal())
                .getUsername())
                .getId();

        List<GroupStudent> groupStudents = groupStudentService
                .getGroupStudents()
                .stream()
                .filter(gs -> gs.getUserId().equals(userId))
                .collect(Collectors.toList());
        GroupStudent groupStudent = groupStudents.get(0);
        Long studentId = groupStudent.getId();

        List<Grade> gradesList = gradeService
                .getGrades()
                .stream()
                .filter(grade -> grade.getGroupStudentId().equals(studentId.toString()))
                .collect(Collectors.toList());

        model.addAttribute("grades", gradesList);
        model.addAttribute("user",
                userService.getUserByLogin(((UserDetails) authentication.getPrincipal())
                        .getUsername()));

        List<Lesson> lessonsList = lessonService.getLessons();
        HashMap<String, Lesson> lessons = new HashMap<>();
        lessonsList.forEach(lesson -> {
            lessons.put(lesson.getId().toString(), lesson);
        });
        model.addAttribute("lessons", lessons);

        List<Course> coursesList = courseService.getCourses();
        HashMap<String, Course> allCourses = new HashMap<>();
        coursesList.forEach(course -> {
            allCourses.put(course.getId().toString(), course);
        });
        model.addAttribute("courses", allCourses);
        return "/grade/student";
    }

    /**
     * CreateForm class.
     */
    @Getter
    @Setter
    public static class CreateForm {

        /**
         * id оценки.
         */
        private Long id;

        /**
         * Идентификатор урока.
         */
        @NotEmpty
        @NotNull
        private String lessonId;

        /**
         * Идентификатор группа/студент.
         */
        @NotNull
        private String groupStudentId;

        /**
         * Оценка.
         */
        private String grade;
    }
}
