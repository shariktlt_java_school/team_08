package ru.edu.project.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.edu.project.authorization.FrontendUserService;
import ru.edu.project.backend.api.courses.CourseService;

import java.util.Collection;

@Controller
public class IndexController {
    /**
     * Grade service.
     */
    @Autowired
    private CourseService courseService;

    /**
     * Зависимость.
     */
    @Autowired
    private FrontendUserService userServiceDa;

    /**
     * Логгер.
     */
    private static final Logger LOG = LoggerFactory.getLogger(IndexController.class);

    /**
     * Точка входа.
     *
     * @param model
     * @param authentication
     * @return view
     */
    @GetMapping("/")
    public String index(final Model model, final Authentication authentication) {
        if (authentication != null && authentication.isAuthenticated()) {
            return redirectByRole(model, authentication.getAuthorities());
        }
        if (courseService.getCourses() != null) { // здесь нужно сделать проверку, что данные пришли
            model.addAttribute("courses",
                    courseService.getCourses());
        }
        return "menu";
    }

    private String redirectByRole(final Model model, final Collection<? extends GrantedAuthority> authorities) {
        model.addAttribute("courses",
                courseService.getCourses());
        return "menu";
    }
}

