package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;

@Repository
public interface GroupEntityRepository extends
        PagingAndSortingRepository<GroupEntity, Long>,
        JpaSpecificationExecutor<GroupEntity> {

    /**
     *
     * @param courseId
     * @return GroupEntity object
     */
    Iterable<GroupEntity> findByCourseId(Long courseId);


}
