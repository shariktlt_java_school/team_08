package ru.edu.project.backend.da;

import ru.edu.project.backend.api.students.User;

import java.util.List;

public interface UserDALayer {
    /**
     * Получение списка пользователей.
     * @return list
     */
    List<User> getUsers();

    /**
     * Получение пользователя по id.
     * @param id
     * @return user
     */
    User getById(Long id);

    /**
     * Создание или обновление юзера.
     * @param user
     * @return user
     */
    User save(User user);

    /**
     * Получение пользователя по логину.
     * @param login
     * @return user
     */
    User getByLogin(String login);
}
