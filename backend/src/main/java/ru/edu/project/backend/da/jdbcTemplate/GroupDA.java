package ru.edu.project.backend.da.jdbcTemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.da.GroupDALayer;

import java.util.List;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class GroupDA implements GroupDALayer {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupDA.class);

    /**
     * Выбрать все группы.
     */
    public static final String SQL_SELECT_ALL =
            "SELECT * FROM groups";

    /**
     * Выбрать все группы, отсортированные по названию курса и названию группы.
     */
    public static final String SQL_SELECT_ALL_SORT_BY_TITLE =
            "SELECT groups.* FROM groups"
                    + " INNER JOIN courses ON groups.group_course_id = courses.id"
                    + " ORDER BY courses.title, groups.group_title";

    /**
     * Удалить все группы.
     */
    public static final String SQL_DELETE_ALL =
            "DELETE FROM groups";

    /**
     * Выбрать группу по id.
     */
    public static final String SQL_WHERE_ID =
            " WHERE group_id = ?";

    /**
     * Выбрать группы по id курса.
     */
    public static final String SQL_WHERE_COURSE_ID =
            " WHERE group_course_id = ?";

    /**
     * Обновить данные группы.
     */
    public static final String SQL_UPDATE_BY_ID =
            "UPDATE groups SET "
                    + "group_course_id = :group_course_id, "
                    + "group_title = :group_title, "
                    + "group_is_started = :group_is_started"
                            + " WHERE group_id = :group_id";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean SimpleJdbcInsert Bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("groups")
                .usingGeneratedKeyColumns("group_id");
    }

    /**
     * Save Group's info.
     * @param group Group object
     * @return Group object
     */
    @Override
    public Group save(final Group group) {
        if (group.getId() == null) {
            return insert(group);
        }
        return update(group);
    }

    private Group update(final Group group) {
        jdbcNamed.update(SQL_UPDATE_BY_ID, toMap(group));
        return group;
    }

    private Group insert(final Group group) {
        Long id = jdbcInsert.executeAndReturnKey(toMap(group)).longValue();
        group.setId(id);
        return group;
    }

    /**
     * Get Group by id.
     * @param id Group id
     * @return Group object
     */
    @Override
    public Group getById(final Long id) {
        return jdbcTemplate.query(SQL_SELECT_ALL + SQL_WHERE_ID,
                this::singleRowMapper,
                id);

    }

    /**
     * @param id Group id
     * @return true if Group was deleted successfully
     */
    @Override
    public Boolean deleteById(final Long id) {
        int result = jdbcTemplate.update(SQL_DELETE_ALL + SQL_WHERE_ID, id);
        return result == 1;
    }

    /**
     * @return List of Group objects
     */
    @Override
    public List<Group> getGroups() {
        return jdbcTemplate.query(SQL_SELECT_ALL_SORT_BY_TITLE,
                this::rowMapper);
    }

    /**
     * @param courseId Course Id
     * @return List of Group objects
     */
    @Override
    public List<Group> getGroupsByCourse(final Long courseId) {
        return jdbcTemplate.query(
                SQL_SELECT_ALL + SQL_WHERE_COURSE_ID,
                this::rowMapper,
                courseId);

    }

    private Map<String, Object> toMap(final Group group) {
        HashMap<String, Object> map = new HashMap<>();

        if (group.getId() != null) {
            map.put("group_id", group.getId());
        }

        map.put("group_course_id", group.getCourseId());
        map.put("group_title", group.getTitle());
        map.put("group_is_started", group.isStarted());

        return map;
    }

    @SneakyThrows
    private Group rowMapper(final ResultSet rs,
                            final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private Group singleRowMapper(final ResultSet rs) {
        if (rs.next()) {
            return mapRow(rs);
        } else {
            return null;
        }
    }

    private Group mapRow(final ResultSet rs) throws SQLException {
        return Group.builder()
                .id(rs.getLong("group_id"))
                .courseId(rs.getLong("group_course_id"))
                .title(rs.getString("group_title"))
                .started(rs.getBoolean("group_is_started"))
                .build();
    }

}
