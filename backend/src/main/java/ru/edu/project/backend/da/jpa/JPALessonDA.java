package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.da.LessonDALayer;
import ru.edu.project.backend.da.jpa.converter.LessonMapper;
import ru.edu.project.backend.da.jpa.entity.LessonEntity;
import ru.edu.project.backend.da.jpa.repository.LessonEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@Profile("SPRING_DATA")
public class JPALessonDA implements LessonDALayer {

    /**
     * Репозиторий.
     */
    @Autowired
    private LessonEntityRepository repository;

    /**
     * Мапер.
     */
    @Autowired
    private LessonMapper mapper;

    /**
     * Получение списка уроков.
     *
     * @return list
     */
    @Override
    public List<Lesson> getLessons() {
        return mapper.mapList(repository.findAll());
    }

    /**
     * Получение урока по id.
     *
     * @param id
     * @return Lesson
     */
    @Override
    public Lesson getById(final Long id) {
        Optional<LessonEntity> lesson = repository.findById(id);
        return lesson.map(entity -> mapper.map(entity)).orElse(null);
    }

    /**
     * Создание или обновление урока.
     *
     * @param lesson
     * @return Lesson
     */
    @Override
    public Lesson save(final Lesson lesson) {
        LessonEntity entity = mapper.map(lesson);
        LessonEntity saved = repository.save(entity);
        return mapper.map(saved);
    }
}
