package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.CourseEntity;

@Repository
public interface CourseEntityRepository
        extends PagingAndSortingRepository<CourseEntity, Long>, JpaSpecificationExecutor<CourseEntity> {
}
