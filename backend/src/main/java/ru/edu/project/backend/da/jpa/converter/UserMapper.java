package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.da.jpa.entity.UserEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    /**
     * @param user
     * @return user.
     */
    User map(UserEntity user);

    /**
     * @param user
     * @return user.
     */
    UserEntity map(User user);

    /**
     * @param users
     * @return list
     */
    List<User> mapList(List<UserEntity> users);
}
