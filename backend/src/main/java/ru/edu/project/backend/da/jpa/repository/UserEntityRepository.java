package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.UserEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserEntityRepository extends
        PagingAndSortingRepository<UserEntity, Long>,
        JpaSpecificationExecutor<UserEntity> {

    /**
     * getAllUsers.
     * @return list
     */
    List<UserEntity> findAll();

    /**
     *
     * @param login
     * @return user
     */
    Optional<UserEntity> findByLogin(String login);
}
