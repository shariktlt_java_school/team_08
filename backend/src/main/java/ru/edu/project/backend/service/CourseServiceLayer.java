package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseForm;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.backend.da.CourseDALayer;

import java.util.List;

@Service
@Profile("!STUB")
public class CourseServiceLayer implements CourseService {

    /**
     * Зависимость для слоя доступа к курсам.
     */
    @Autowired
    private CourseDALayer daLayer;

    /**
     * Создание курса.
     *
     * @param form - CourseForm
     * @return Course.
     */
    @Override
    public Course createCourse(final CourseForm form) {
        Course course = Course.builder().
                title(form.getTitle()).
                description(form.getDescription())
                .teacherId(form.getTeacherId())
                .build();
        return daLayer.save(course);
    }

    /**
     * Сохранение существующего курса после редактирования.
     *
     * @param form - CourseForm
     * @return Course.
     */
    @Override
    public Course saveCourse(final CourseForm form) {
        Course course = Course.builder().
                id(form.getId())
                .title(form.getTitle())
                .description(form.getDescription())
                .teacherId(form.getTeacherId())
                .build();
        return daLayer.save(course);
    }

    /**
     * Получить список курсов.
     *
     * @return List<Course>
     */
    @Override
    public List<Course> getCourses() {
        return daLayer.getCourses();
    }

    /**
     * Получить курс по id.
     *
     * @param id id
     * @return List<Course>
     */
    @Override
    public Course getCourse(final Long id) {
        return daLayer.getById(id);
    }
}
