package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupMapper {

    /**
     * @param group
     * @return Group object
     */
    Group map(GroupEntity group);

    /**
     * @param group
     * @return GroupEntity object.
     */
    GroupEntity map(Group group);

    /**
     * @param groups
     * @return List of Group objects
     */
    List<Group> mapList(Iterable<GroupEntity> groups);

}
