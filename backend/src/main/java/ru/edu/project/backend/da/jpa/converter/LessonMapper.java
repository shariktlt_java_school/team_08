package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.da.jpa.entity.LessonEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LessonMapper {

    /**
     * @param lesson
     * @return Lesson.
     */
    Lesson map(LessonEntity lesson);

    /**
     * @param lesson
     * @return Lesson.
     */
    LessonEntity map(Lesson lesson);

    /**
     * @param lessons
     * @return list
     */
    List<Lesson> mapList(Iterable<LessonEntity> lessons);
}
