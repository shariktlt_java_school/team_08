package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "group_students")
public class GroupStudentEntity {

    /**
     * Student's id in his group.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "group_students_id_sequence")
    @SequenceGenerator(name = "group_students_id_sequence",
            sequenceName = "group_students_id_sequence",
            allocationSize = 1)
    @Column(name = "group_student_id")
    private Long id;

    /**
     * Group's id.
     */
    @Column(name = "group_student_group_id")
    private Long groupId;

    /**
     * Student's user_id.
     */
    @Column(name = "group_student_user_id")
    private Long userId;

}
