package ru.edu.project.backend.da;

import ru.edu.project.backend.api.lessons.Lesson;

import java.util.List;

public interface LessonDALayer {

    /**
     * Получение списка уроков.
     *
     * @return list
     */
    List<Lesson> getLessons();

    /**
     * Получение урока по id.
     *
     * @param id
     * @return Lesson
     */
    Lesson getById(Long id);

    /**
     * Создание или обновление урока.
     *
     * @param course
     * @return Course
     */
    Lesson save(Lesson course);
}
