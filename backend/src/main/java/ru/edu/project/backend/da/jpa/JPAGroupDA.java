package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.da.GroupDALayer;
import ru.edu.project.backend.da.jpa.converter.GroupMapper;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;
import ru.edu.project.backend.da.jpa.repository.GroupEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@Profile("SPRING_DATA")
public class JPAGroupDA implements GroupDALayer {


    /**
     * Репозиторий.
     */
    @Autowired
    private GroupEntityRepository repository;

    /**
     * Мапер.
     */
    @Autowired
    private GroupMapper mapper;


    /**
     * Save Group's info.
     *
     * @param group Group object
     * @return Group object
     */
    @Override
    public Group save(final Group group) {
        GroupEntity entity = mapper.map(group);
        GroupEntity saved = repository.save(entity);
        return mapper.map(saved);
    }

    /**
     * Get Group by id.
     *
     * @param id Group id
     * @return Group object
     */
    @Override
    public Group getById(final Long id) {
        Optional<GroupEntity> group = repository.findById(id);
        return group.map(entity -> mapper.map(entity)).orElse(null);
    }

    /**
     * Delete Group by id.
     *
     * @param id Group id
     * @return Group was deleted?
     */
    @Override
    public Boolean deleteById(final Long id) {
        Optional<GroupEntity> group = repository.findById(id);
        if (group.isPresent()) {
            repository.delete(group.get());
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get all Groups from database.
     *
     * @return List of Group objects
     */
    @Override
    public List<Group> getGroups() {
        return mapper.mapList(repository.findAll());
    }

    /**
     * Get Groups by Course id.
     *
     * @param courseId Course id
     * @return List of Group objects
     */
    @Override
    public List<Group> getGroupsByCourse(final Long courseId) {
        Iterable<GroupEntity> groups = repository.findByCourseId(courseId);
        return mapper.mapList(groups);
    }
}
