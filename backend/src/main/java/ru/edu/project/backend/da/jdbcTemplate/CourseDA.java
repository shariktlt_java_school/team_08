package ru.edu.project.backend.da.jdbcTemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.da.CourseDALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class CourseDA implements CourseDALayer {

    /**
     * Запрос - получить все курсы.
     */
    public static final String SELECT_ALL = "select * from courses";


    /**
     * Запрос - получить курс по id.
     */
    public static final String SELECT_BY_ID = "select * from courses where id = ?";

    /**
     * Найти курс по id и обновить данные.
     */
    public static final String UPDATE = "update courses "
            + "set title = :title, "
            + "course_description = :course_description, "
            + "course_teacher_user_id = :course_teacher_user_id "
            + "where id = :id";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("courses")
                .usingGeneratedKeyColumns("id");
    }

    /**
     * Получение списка курсов.
     *
     * @return list
     */
    @Override
    public List<Course> getCourses() {
        return jdbcTemplate.query(SELECT_ALL, this::rowMapper);
    }

    /**
     * Получение курса по id.
     *
     * @param id
     * @return user
     */
    @Override
    public Course getById(final Long id) {
        return jdbcTemplate.query(SELECT_BY_ID, this::singleRowMapper, id);
    }

    /**
     * Создание или обновление курса.
     *
     * @param course
     * @return Course
     */
    @Override
    public Course save(final Course course) {
        if (course.getId() == null) {
            return insert(course);
        } else {
            return update(course);
        }
    }

    private Course update(final Course course) {
        jdbcNamed.update(UPDATE, toMap(course));
        return course;
    }

    private Course insert(final Course course) {
        Long id = jdbcInsert.executeAndReturnKey(toMap(course)).longValue();
        course.setId(id);
        return course;
    }

    private Map<String, Object> toMap(final Course course) {
        HashMap<String, Object> map = new HashMap<>();
        if (course.getId() != null) {
            map.put("id", course.getId());
        }
        map.put("title", course.getTitle());
        map.put("course_description", course.getDescription());
        map.put("course_teacher_user_id", course.getTeacherId());
        return map;
    }

    @SneakyThrows
    private Course rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private Course singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private Course mapRow(final ResultSet rs) throws SQLException {
        return Course.builder()
                .id(rs.getLong("id"))
                .title(rs.getString("title"))
                .description(rs.getString("course_description"))
                .teacherId(rs.getLong("course_teacher_user_id"))
                .build();
    }
}
