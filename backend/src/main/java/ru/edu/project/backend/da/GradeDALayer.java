package ru.edu.project.backend.da;

import ru.edu.project.backend.api.grades.Grade;

import java.util.List;

public interface GradeDALayer {
    /**
     * Получение списка оценок.
     * @return list
     */
    List<Grade> getGrades();

    /**
     * Получение оценки по id.
     * @param id
     * @return grade
     */
    Grade getById(Long id);

    /**
     * Создание или обновление оценки.
     * @param grade
     * @return grade
     */
    Grade save(Grade grade);

    /**
     * Delete Grade by id.
     * @param id Grade id
     * @return Grade was deleted?
     */
    Boolean deleteById(Long id);

    /**
     * Delete All Grades.
     *
     * @return Grade was deleted?
     */
    Boolean deleteAll();

}
