package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.da.CourseDALayer;
import ru.edu.project.backend.da.jpa.converter.CourseMapper;
import ru.edu.project.backend.da.jpa.entity.CourseEntity;
import ru.edu.project.backend.da.jpa.repository.CourseEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@Profile("SPRING_DATA")
public class JPACourseDA implements CourseDALayer {

    /**
     * Репозиторий.
     */
    @Autowired
    private CourseEntityRepository repository;

    /**
     * Мапер.
     */
    @Autowired
    private CourseMapper mapper;

    /**
     * Получение списка куросв.
     *
     * @return list
     */
    @Override
    public List<Course> getCourses() {
        return mapper.mapList(repository.findAll());
    }

    /**
     * Получение курса по id.
     *
     * @param id
     * @return course
     */
    @Override
    public Course getById(final Long id) {
        Optional<CourseEntity> course = repository.findById(id);
        return course.map(entity -> mapper.map(entity)).orElse(null);
    }

    /**
     * Создание или обновление курса.
     *
     * @param course
     * @return course
     */
    @Override
    public Course save(final Course course) {
        CourseEntity entity = mapper.map(course);
        CourseEntity saved = repository.save(entity);
        return mapper.map(saved);
    }
}
