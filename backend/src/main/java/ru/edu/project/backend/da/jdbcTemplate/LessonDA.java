package ru.edu.project.backend.da.jdbcTemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.da.LessonDALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class LessonDA implements LessonDALayer {

    /**
     * Запрос - получить все уроки.
     */
    public static final String SELECT_ALL = "select * from lessons";


    /**
     * Запрос - получить урок по id.
     */
    public static final String SELECT_BY_ID = "select * from lessons where id = ?";

    /**
     * Найти урок по id и обновить данные.
     */
    public static final String UPDATE = "update lessons "
            + "set title = :title, "
            + "lesson_course_id = :lesson_course_id, "
            + "lesson_title = :lesson_title "
            + "lesson_description = :lesson_description "
            + "lesson_homework = :lesson_homework "
            + "where id = :id";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    //@Autowired
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("lessons")
                .usingGeneratedKeyColumns("id");
    }

    /**
     * Получение списка уроков.
     *
     * @return list
     */
    @Override
    public List<Lesson> getLessons() {
        return jdbcTemplate.query(SELECT_ALL, this::rowMapper);
    }

    /**
     * Получение урока по id.
     *
     * @param id
     * @return user
     */
    @Override
    public Lesson getById(final Long id) {
        return jdbcTemplate.query(SELECT_BY_ID, this::singleRowMapper, id);
    }

    /**
     * Создание или обновление урока.
     *
     * @param lesson
     * @return user
     */
    @Override
    public Lesson save(final Lesson lesson) {
        if (lesson.getId() == null) {
            return insert(lesson);
        } else {
            return update(lesson);
        }
    }

    private Lesson update(final Lesson lesson) {
        jdbcNamed.update(UPDATE, toMap(lesson));
        return lesson;
    }

    private Lesson insert(final Lesson lesson) {
        Long id = jdbcInsert.executeAndReturnKey(toMap(lesson)).longValue();
        lesson.setId(id);
        return lesson;
    }

    private Map<String, Object> toMap(final Lesson lesson) {
        HashMap<String, Object> map = new HashMap<>();
        if (lesson.getId() != null) {
            map.put("id", lesson.getId());
        }
        map.put("lesson_course_id", lesson.getCourseId());
        map.put("lesson_title", lesson.getTitle());
        map.put("lesson_description", lesson.getDescription());
        map.put("lesson_homework", lesson.getHomework());
        return map;
    }

    @SneakyThrows
    private Lesson rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private Lesson singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private Lesson mapRow(final ResultSet rs) throws SQLException {
        return Lesson.builder()
                .id(rs.getLong("id"))
                .courseId(rs.getLong("lesson_course_id"))
                .title(rs.getString("lesson_title"))
                .description(rs.getString("lesson_description"))
                .homework(rs.getString("lesson_homework"))
                .build();
    }
}
