package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseForm;
import ru.edu.project.backend.api.courses.CourseService;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController implements CourseService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private CourseService delegate;

    /**
     * Создание курса.
     *
     * @param form - CourseForm
     * @return Course.
     */
    @Override
    @PostMapping("/createCourse")
    public Course createCourse(@RequestBody final CourseForm form) {
        return delegate.createCourse(form);
    }

    /**
     * Сохранение существующего курса после редактирования.
     *
     * @param form - CourseForm
     * @return Course
     */
    @Override
    @PostMapping("/saveCourse")
    public Course saveCourse(@RequestBody final CourseForm form) {
        return delegate.saveCourse(form);
    }

    /**
     * Получить список курсов.
     *
     * @return List<Course>
     */
    @Override
    @GetMapping(value = "/getCourses")
    public List<Course> getCourses() {
        return delegate.getCourses();
    }

    /**
     * Получить курс по id.
     *
     * @param id id
     * @return List<Course>
     */
    @Override
    @GetMapping("/getCourse/{id}")
    public Course getCourse(@PathVariable("id") final Long id) {
        return delegate.getCourse(id);
    }
}
