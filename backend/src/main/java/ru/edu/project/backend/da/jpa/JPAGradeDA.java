package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.da.GradeDALayer;
import ru.edu.project.backend.da.jpa.converter.GradeMapper;
import ru.edu.project.backend.da.jpa.entity.GradeEntity;
import ru.edu.project.backend.da.jpa.repository.GradeEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@Profile("SPRING_DATA")
public class JPAGradeDA implements GradeDALayer {

    /**
     * Репозиторий.
     */
    @Autowired
    private GradeEntityRepository repository;

    /**
     * Маппер.
     */
    @Autowired
    private GradeMapper mapper;

    /**
     * Получение списка оценок.
     *
     * @return list
     */
    @Override
    public List<Grade> getGrades() {
        return mapper.mapList(repository.findAll());
    }

    /**
     * Получение оценки по id.
     *
     * @param id
     * @return grade
     */
    @Override
    public Grade getById(final Long id) {
        //Optional<GradeEntity> grade = repository.findById(Long.parseLong(id));
        System.out.println("_____JPA_GRADE_BY_ID_id: " + id);
        Optional<GradeEntity> grade = repository.findById(id);
        return grade.map(entity -> mapper.map(entity)).orElse(null);
    }

    private Grade insert(final Grade grade) {
        System.out.println("_____JPA_GRADE_INSERT_grade: " + grade);
        return null;
    }

    /**
     * Создание или обновление оценки.
     *
     * @param grade
     * @return grade
     */
    @Override
    public Grade save(final Grade grade) {
        System.out.println("_____JPA_GRADE_SAVE_grade: " + grade);
        GradeEntity entity = mapper.map(grade);
        GradeEntity saved = repository.save(entity);
        return mapper.map(saved);
    }

    /**
     * Delete Grade by id.
     *
     * @param id Grade id
     * @return Grade was deleted?
     */
    @Override
    public Boolean deleteById(final Long id) {
        System.out.println("_____JPA_GRADE_DELETE_id: " + id);
        repository.deleteById(id);
        return true;
    }

    /**
     * Delete All Grades.
     *
     * @return Grade was deleted?
     */
    @Override
    public Boolean deleteAll() {
        System.out.println("_____JPA_GRADE_DELETE_all");
        return null;
    }
}
