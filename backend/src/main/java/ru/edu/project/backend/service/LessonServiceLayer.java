package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonForm;
import ru.edu.project.backend.api.lessons.LessonService;
import ru.edu.project.backend.da.LessonDALayer;

import java.util.List;

@Service
@Profile("!STUB")
public class LessonServiceLayer implements LessonService {

    /**
     * Зависимость для слоя доступа к урокам.
     */
    @Autowired
    private LessonDALayer daLayer;

    /**
     * Создание урока.
     *
     * @param form - LessonForm
     * @return Lesson.
     */
    @Override
    public Lesson createLesson(final LessonForm form) {
        Lesson lesson = Lesson.builder()
                .courseId(form.getCourseId())
                .title(form.getTitle())
                .description(form.getDescription())
                .homework(form.getHomework())
                .build();
        return daLayer.save(lesson);
    }

    /**
     * Сохранение существующего урока после редактирования.
     *
     * @param form - LessonForm
     * @return Lesson
     */
    @Override
    public Lesson saveLesson(final LessonForm form) {
        Lesson lesson = Lesson.builder()
                .id(form.getId())
                .courseId(form.getCourseId())
                .title(form.getTitle())
                .description(form.getDescription())
                .homework(form.getHomework())
                .build();
        return daLayer.save(lesson);
    }

    /**
     * Получить список уроков.
     *
     * @return List<Lesson>
     */
    @Override
    public List<Lesson> getLessons() {
        return daLayer.getLessons();
    }

    /**
     * Получить урок по id.
     *
     * @param id id
     * @return List<Lesson>
     */
    @Override
    public Lesson getLesson(final Long id) {
        return daLayer.getById(id);
    }
}
