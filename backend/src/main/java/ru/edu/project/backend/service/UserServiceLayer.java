package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserForm;
import ru.edu.project.backend.api.students.UserService;
import ru.edu.project.backend.da.UserDALayer;

import java.util.List;

@Service
@Profile("!STUB")
//@Qualifier("RequestServiceLayer")
public class UserServiceLayer implements UserService {

    /**
     * Зависимость для слоя доступа к данным заявок.
     */
    @Autowired
    private UserDALayer daLayer;

    /**
     * создание студента через .builder.
     *
     * @param form
     * @return new student
     */
    @Override
    public User createUser(final UserForm form) {
        User user = User.builder().
                name(form.getName()).
                surname(form.getSurname()).
                middleName(form.getMiddleName()).
                role(form.getRole()).
                login(form.getLogin()).
                password(form.getPassword()).
                build();
        return daLayer.save(user);
    }

    /**
     * @return list
     */
    @Override
    public List<User> getUsers() {
        return daLayer.getUsers();
    }

    /**
     * @param id
     * @param form
     * @return updated
     */
    @Override
    public User updateUser(final Long id, final UserForm form) {
        User user = User.builder().
                name(form.getName()).
                surname(form.getSurname()).
                middleName(form.getMiddleName()).
                role(form.getRole()).build();
        user.setId(id);
        return daLayer.save(user);
    }

    /**
     * @param login
     * @return user by login
     */
    @Override
    public User getUserByLogin(final String login) {
        return daLayer.getByLogin(login);
    }
}
