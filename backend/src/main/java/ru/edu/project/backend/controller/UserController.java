package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserForm;
import ru.edu.project.backend.api.students.UserService;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController implements UserService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private UserService delegate;

    /**
     * создание пользователя через .builder.
     *
     * @param form
     * @return new student
     */
    @Override
//    @PostMapping("/create")
    @PostMapping("/createUser")
    public User createUser(@RequestBody final UserForm form) {
        return delegate.createUser(form);
    }

    /**
     * @return list
     */
    @Override
//    @GetMapping("/all")
    @GetMapping("/getUsers")
    public List<User> getUsers() {
        return delegate.getUsers();
    }

    /**
     * @param id
     * @param form
     * @return updated
     */
    @Override
//    @PostMapping("/{id}")
//    @PostMapping("/update/{id}")
    @PostMapping("/updateUser/{id}")
    public User updateUser(@PathVariable("id") final Long id, @RequestBody final UserForm form) {
        return delegate.updateUser(id, form);
    }

    /**
     * @param login
     * @return user by login
     */
    @Override
    @GetMapping("/getUserByLogin/{login}")
    public User getUserByLogin(@PathVariable("login") final String login) {
        return delegate.getUserByLogin(login);
    }
}
