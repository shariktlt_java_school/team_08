package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;

import java.util.List;

@RestController
@RequestMapping("/group")
public class GroupController implements GroupService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private GroupService delegate;

    /**
     * Creating group.
     *
     * @param form Data from Group form
     * @return new Group object
     */
    @Override
    @PostMapping("/createGroup")
    public Group createGroup(@RequestBody final GroupForm form) {
        return delegate.createGroup(form);
    }

    /**
     * Get all groups from database.
     * @return list of Group objects
     */
    @Override
    @GetMapping("/getGroups")
    public List<Group> getGroups() {
        return delegate.getGroups();
    }

    /**
     * Get groups list by course id.
     * @param courseId Course Id
     * @return list of Group objects
     */
    @Override
    @GetMapping("/getByCourse/{courseId}")
    public List<Group> getByCourse(@PathVariable("courseId") final Long courseId) {
        return delegate.getByCourse(courseId);
    }

    /**
     * Update Group info.
     * @param id Group id
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    @PostMapping("/updateGroup/{id}")
    public Group updateGroup(@PathVariable("id") final Long id,
                             @RequestBody final GroupForm form) {
        return delegate.updateGroup(id, form);
    }

    /**
     * Update Group info.
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    @PostMapping("/updateGroup")
    public Group updateGroup(@RequestBody final GroupForm form) {
        return delegate.updateGroup(form.getId(), form);
    }

    /**
     * Get Group's info.
     * @param id Group id
     * @return Group object
     */
    @Override
    @GetMapping("/getGroup/{id}")
    public Group getGroup(@PathVariable("id") final Long id) {
        return delegate.getGroup(id);
    }

    /**
     * Delete Group by id.
     * @param id Group id
     * @return Group was deleted?
     */
    @Override
    @PostMapping("/deleteGroup/{id}")
    public boolean deleteGroup(@PathVariable("id") final Long id) {
        return delegate.deleteGroup(id);
    }

}
