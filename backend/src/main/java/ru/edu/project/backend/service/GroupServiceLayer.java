package ru.edu.project.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.da.GroupDALayer;

import java.util.List;

@Service
@Profile("!STUB")
//@Qualifier("RequestServiceLayer")
public class GroupServiceLayer implements GroupService {


    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupServiceLayer.class);


    /**
     * Зависимость для слоя доступа к данным заявок.
     */
    @Autowired
    private GroupDALayer daLayer;

    /**
     * Creating Group.
     *
     * @param form Data from Group form
     * @return new Group object
     */
    @Override
    public Group createGroup(final GroupForm form) {
        Group group = Group.builder()
                .courseId(form.getCourseId())
                .title(form.getTitle())
                .started(form.isStarted())
                .build();
        return daLayer.save(group);
    }

    /**
     * Update Group info.
     * @param id Group id
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    public Group updateGroup(final Long id,
                             final GroupForm form) {

        Group group = Group.builder()
                .courseId(form.getCourseId())
                .title(form.getTitle())
                .started(form.isStarted())
                .build();
        group.setId(id);
        return daLayer.save(group);
    }

    /**
     * Update Group info.
     *
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    public Group updateGroup(final GroupForm form) {
        return updateGroup(form.getId(), form);
    }

    /**
     * Get Group's info.
     * @param id Group id
     * @return Group object
     */
    @Override
    public Group getGroup(final Long id) {
        return daLayer.getById(id);
    }

    /**
     * Delete Group by id.
     * @param id Group id
     * @return Group was deleted?
     */
    @Override
    public boolean deleteGroup(final Long id) {
        return daLayer.deleteById(id);
    }

    /**
     * @return list
     */
    @Override
    public List<Group> getGroups() {
        return daLayer.getGroups();
    }

    /**
     * @param courseId Course id
     * @return list
     */
    @Override
    public List<Group> getByCourse(final Long courseId) {
        return daLayer.getGroupsByCourse(courseId);
    }

}
