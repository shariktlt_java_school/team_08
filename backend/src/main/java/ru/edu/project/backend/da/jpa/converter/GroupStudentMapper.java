package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.da.jpa.entity.GroupStudentEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupStudentMapper {

    /**
     * @param group
     * @return Group object
     */
    GroupStudent map(GroupStudentEntity group);

    /**
     * @param group
     * @return GroupEntity object.
     */
    GroupStudentEntity map(GroupStudent group);

    /**
     * @param groups
     * @return List of Group objects
     */
    List<GroupStudent> mapList(Iterable<GroupStudentEntity> groups);

}
