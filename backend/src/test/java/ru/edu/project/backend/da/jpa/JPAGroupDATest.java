package ru.edu.project.backend.da.jpa;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.app.BackendApplication;
import ru.edu.project.backend.da.jpa.converter.GroupMapper;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;
import ru.edu.project.backend.da.jpa.entity.UserEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("SPRING_DATA")
@RunWith(SpringRunner.class)
@DataJpaTest // автоматически создает конфигурцию для hibernate h2
@ContextConfiguration(classes = BackendApplication.class) // указывает где искать конфигурационный класс для спринг бут
public class JPAGroupDATest {

    public static final long GROUP_COURSE_ID = 1L;
    public static final String GROUP_TITLE = "TU-1";

    public static final long NEXT_GROUP_COURSE_ID = 2L;
    public static final String NEXT_GROUP_TITLE = "UT-2";

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private JPAGroupDA repository;

    private final GroupMapper mapper = Mappers.getMapper(GroupMapper.class);

    @Test
    public void update() {
        Group group = Group
                .builder()
                .courseId(GROUP_COURSE_ID)
                .title(GROUP_TITLE)
                .started(true)
                .build();

        Group newGroup = repository.save(group);
        assertNotNull(newGroup.getId());

        GroupEntity entity = manager.find(GroupEntity.class, newGroup.getId());
        assertEquals(group.getCourseId(), entity.getCourseId());
        assertEquals(group.getTitle(), entity.getTitle());
        assertEquals(group.isStarted(), entity.isStarted());

    }


    @Test
    public void getById() {
        Group group =
            Group.builder()
                .courseId(GROUP_COURSE_ID)
                .title(GROUP_TITLE)
                .started(true)
                .build();

        GroupEntity newGroup = manager.persist(mapper.map(group));
        Group foundGroup = repository.getById(newGroup.getId());

        assertNotNull(foundGroup);
        assertEquals(group.getCourseId(), foundGroup.getCourseId());
        assertEquals(group.getTitle(), foundGroup.getTitle());
        assertEquals(group.isStarted(), foundGroup.isStarted());

    }


    @Test
    public void getGroups() {
        setRepository();

        List<Group> foundGroups = repository.getGroups();
        assertEquals(2, foundGroups.size());

    }

    @Test
    public void getGroupsByCourse() {

        setRepository();

        List<Group> foundGroupsByCourse = repository.getGroupsByCourse(GROUP_COURSE_ID);
        assertEquals(1, foundGroupsByCourse.size());

        foundGroupsByCourse = repository.getGroupsByCourse(NEXT_GROUP_COURSE_ID);
        assertEquals(1, foundGroupsByCourse.size());

    }

    @Test
    public void deleteById() {

        setRepository();

        List<Group> foundGroups = repository.getGroups();
        Boolean result = repository.deleteById(foundGroups.get(0).getId());
        assertTrue(result);

    }


    private void setRepository() {
        Group group1 =
            Group.builder()
                .courseId(GROUP_COURSE_ID)
                .title(GROUP_TITLE)
                .started(true)
                .build();
        Group group2 =
            Group.builder()
                .courseId(NEXT_GROUP_COURSE_ID)
                .title(NEXT_GROUP_TITLE)
                .started(true)
                .build();

        manager.persist(mapper.map(group1));
        manager.persist(mapper.map(group2));
    }


}
