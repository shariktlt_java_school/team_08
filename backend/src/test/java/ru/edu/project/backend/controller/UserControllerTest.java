package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserForm;
import ru.edu.project.backend.api.students.UserService;

import java.util.Arrays;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController controller;

    private User mockUser = User.builder().name("1").role(User.UserRole.Student).build();

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getUsers() throws Exception {
        when(userService.getUsers()).thenReturn(Arrays.asList(mockUser));
        RequestBuilder builder = MockMvcRequestBuilders.get("/user/getUsers").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "[{\"id\":null,\"name\":\"1\",\"surname\":null,\"middleName\":null,\"role\":\"Student\",\"login\":null,\"password\":null}]";
        assertEquals(test, result.getResponse().getContentAsString());
    }

    @Test
    public void createUser() throws Exception {
        when(userService.createUser(any(UserForm.class))).thenReturn(mockUser);
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders.post("/user/createUser").accept(MediaType.APPLICATION_JSON).
                content(json).contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        String test = "{\"id\":null,\"name\":\"1\",\"surname\":null,\"middleName\":null,\"role\":\"Student\",\"login\":null,\"password\":null}";
        assertEquals(test, result.getResponse().getContentAsString());
    }

    @Test
    public void updateUser() throws Exception {
        when(userService.updateUser(any(Long.class), any(UserForm.class))).thenReturn(mockUser);
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders.post("/user/updateUser/1").accept(MediaType.APPLICATION_JSON).
                content(json).contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "{\"id\":null,\"name\":\"1\",\"surname\":null,\"middleName\":null,\"role\":\"Student\",\"login\":null,\"password\":null}";
        String actual = result.getResponse().getContentAsString();
        assertEquals(test, actual);
    }

    @Test
    public void getUserByLogin() throws Exception {
        when(userService.getUserByLogin(any(String.class))).thenReturn(mockUser);
        RequestBuilder builder = MockMvcRequestBuilders.get("/user/getUserByLogin/login").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "{\"id\":null,\"name\":\"1\",\"surname\":null,\"middleName\":null,\"role\":\"Student\",\"login\":null,\"password\":null}";
        String actual = result.getResponse().getContentAsString();
        assertEquals(test, actual);
    }
}