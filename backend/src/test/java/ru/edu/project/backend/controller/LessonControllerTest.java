package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.lessons.LessonForm;
import ru.edu.project.backend.api.lessons.LessonService;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(LessonController.class)
public class LessonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private LessonService lessonService;

    @InjectMocks
    private LessonController controller;

    private Lesson mockLesson = Lesson
            .builder()
            .id(1L)
            .courseId(2L)
            .title("Lesson")
            .description("Description")
            .homework("Homework")
            .build();

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getLessons() throws Exception {
        when(lessonService.getLessons()).thenReturn(Arrays.asList(mockLesson));
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/lesson/getLessons").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "[{\"id\":1,\"courseId\":2,\"title\":\"Lesson\",\"description\":\"Description\",\"homework\":\"Homework\"}]";
        assertEquals(test, result.getResponse().getContentAsString());
    }

    @Test
    public void createLesson() throws Exception {
        when(lessonService.createLesson(any(LessonForm.class))).thenReturn(mockLesson);
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/lesson/createLesson")
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        String test = "{\"id\":1,\"courseId\":2,\"title\":\"Lesson\",\"description\":\"Description\",\"homework\":\"Homework\"}";
        assertEquals(test, result.getResponse().getContentAsString());
    }

    @Test
    public void saveLesson() throws Exception {
        when(lessonService.saveLesson(any(LessonForm.class))).thenReturn(mockLesson);
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/lesson/saveLesson")
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "{\"id\":1,\"courseId\":2,\"title\":\"Lesson\",\"description\":\"Description\",\"homework\":\"Homework\"}";
        String actual = result.getResponse().getContentAsString();
        assertEquals(test, actual);
    }

    @Test
    public void getLessonById() throws Exception {
        when(lessonService.getLesson(any(Long.class))).thenReturn(mockLesson);
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/lesson/getLesson/1")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "{\"id\":1,\"courseId\":2,\"title\":\"Lesson\",\"description\":\"Description\",\"homework\":\"Homework\"}";
        String actual = result.getResponse().getContentAsString();
        assertEquals(test, actual);
    }
}