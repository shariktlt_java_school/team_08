package ru.edu.project.backend.da.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.app.BackendApplication;
import ru.edu.project.backend.da.jpa.converter.GroupStudentMapper;
import ru.edu.project.backend.da.jpa.entity.GroupStudentEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("SPRING_DATA")
@RunWith(SpringRunner.class)
@DataJpaTest // автоматически создает конфигурцию для hibernate h2
@ContextConfiguration(classes = BackendApplication.class) // указывает где искать конфигурационный класс для спринг бут
public class JPAGroupStudentDATest {

    public static final long GROUP_ID = 1L;
    public static final long USER_ID = 1L;

    public static final long NEXT_GROUP_ID = 2L;
    public static final long NEXT_USER_ID = 2L;

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private JPAGroupStudentDA repository;

    private final GroupStudentMapper mapper = Mappers.getMapper(GroupStudentMapper.class);

    @Test
    public void update() {
        GroupStudent groupStudent =
                GroupStudent
                .builder()
                .groupId(GROUP_ID)
                .userId(USER_ID)
                .build();

        GroupStudent newGroupStudent = repository.save(groupStudent);
        assertNotNull(newGroupStudent.getId());

        GroupStudentEntity entity = manager.find(GroupStudentEntity.class, newGroupStudent.getId());
        assertEquals(groupStudent.getGroupId(), entity.getGroupId());
        assertEquals(groupStudent.getUserId(), entity.getUserId());

    }


    @Test
    public void getById() {
        GroupStudent groupStudent =
                GroupStudent.builder()
                        .groupId(GROUP_ID)
                        .userId(USER_ID)
                        .build();

        GroupStudentEntity newGroupStudent = manager.persist(mapper.map(groupStudent));
        GroupStudent foundGroupStudent = repository.getById(newGroupStudent.getId());

        assertNotNull(foundGroupStudent);
        assertEquals(groupStudent.getGroupId(), foundGroupStudent.getGroupId());
        assertEquals(groupStudent.getUserId(), foundGroupStudent.getUserId());

    }


    @Test
    public void getGroupStudents() {
        setRepository();

        List<GroupStudent> foundGroupStudents = repository.getGroupStudents();
        assertEquals(2, foundGroupStudents.size());

    }

    @Test
    public void getByGroup() {

        setRepository();

        List<GroupStudent> foundGroupStudentsByGroup = repository.getByGroup(GROUP_ID);
        assertEquals(1, foundGroupStudentsByGroup.size());

        foundGroupStudentsByGroup = repository.getByGroup(NEXT_GROUP_ID);
        assertEquals(1, foundGroupStudentsByGroup.size());

    }


    private void setRepository() {
        GroupStudent groupStudent1 =
                GroupStudent.builder()
                        .groupId(GROUP_ID)
                        .userId(USER_ID)
                        .build();
        GroupStudent groupStudent2 =
                GroupStudent.builder()
                        .groupId(NEXT_GROUP_ID)
                        .userId(NEXT_USER_ID)
                        .build();

        manager.persist(mapper.map(groupStudent1));
        manager.persist(mapper.map(groupStudent2));
    }


}
