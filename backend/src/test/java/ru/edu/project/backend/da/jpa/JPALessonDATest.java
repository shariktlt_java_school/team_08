package ru.edu.project.backend.da.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.app.BackendApplication;
import ru.edu.project.backend.da.jpa.converter.LessonMapper;
import ru.edu.project.backend.da.jpa.entity.LessonEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("SPRING_DATA")
@RunWith(SpringRunner.class)
@DataJpaTest // автоматически создает конфигурцию для hibernate h2
@ContextConfiguration(classes = BackendApplication.class) // указывает где искать конфигурационный класс для спринг бут
public class JPALessonDATest {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private JPALessonDA repository;

    private final LessonMapper  mapper = Mappers.getMapper(LessonMapper.class);

    @Test
    public void getLessons() {
        Lesson lesson1 = Lesson
                .builder()
                .title("title")
                .description("description")
                .homework("homework")
                .courseId(20L)
                .build();
        Lesson lesson2 = Lesson
                .builder()
                .title("title2")
                .description("description2")
                .homework("homework2")
                .courseId(40L)
                .build();
        manager.persist(mapper.map(lesson1));
        manager.persist(mapper.map(lesson2));
        List<Lesson> foundLessons = repository.getLessons();
        assertEquals(2, foundLessons.size());
    }

    @Test
    public void getById() {
        Lesson lesson = Lesson
                .builder()
                .title("title")
                .description("description")
                .homework("homework")
                .courseId(20L)
                .build();
        manager.persist(mapper.map(lesson));
        Lesson foundLesson = repository.getById(1L);
        assertNotNull(foundLesson);
        assertEquals(lesson.getTitle(), foundLesson.getTitle());
    }

    @Test
    public void save() {
        Lesson lesson = Lesson
                .builder()
                .id(1L)
                .title("title")
                .description("description")
                .homework("homework")
                .courseId(20L)
                .build();
        Lesson newLesson = repository.save(lesson);
        assertNotNull(newLesson.getId());
        LessonEntity entity = manager.find(LessonEntity.class, newLesson.getId());
        assertEquals(lesson.getTitle(), entity.getTitle());
    }
}