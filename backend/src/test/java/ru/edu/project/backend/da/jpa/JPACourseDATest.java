package ru.edu.project.backend.da.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.app.BackendApplication;
import ru.edu.project.backend.da.jpa.converter.CourseMapper;
import ru.edu.project.backend.da.jpa.entity.CourseEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("SPRING_DATA")
@RunWith(SpringRunner.class)
@DataJpaTest // автоматически создает конфигурцию для hibernate h2
@ContextConfiguration(classes = BackendApplication.class) // указывает где искать конфигурационный класс для спринг бут
public class JPACourseDATest {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private JPACourseDA repository;

    private final CourseMapper  mapper = Mappers.getMapper(CourseMapper.class);

    @Test
    public void getCourses() {
        Course course1 = Course
                .builder()
                .title("title")
                .description("description")
                .teacherId(20L)
                .build();
        Course course2 = Course
                .builder()
                .title("title2")
                .description("description2")
                .teacherId(40L)
                .build();
        manager.persist(mapper.map(course1));
        manager.persist(mapper.map(course2));
        List<Course> foundCourses = repository.getCourses();
        assertEquals(2, foundCourses.size());
    }

    @Test
    public void getById() {
        Course course = Course
                .builder()
                .title("title")
                .description("description")
                .teacherId(20L)
                .build();
        manager.persist(mapper.map(course));
        Course foundCourse = repository.getById(1L);
        assertNotNull(foundCourse);
        assertEquals(course.getTitle(), foundCourse.getTitle());
    }

    @Test
    public void save() {
        Course course = Course
                .builder()
                .id(1L)
                .title("title")
                .description("description")
                .teacherId(20L)
                .build();
        Course newCourse = repository.save(course);
        assertNotNull(newCourse.getId());
        CourseEntity entity = manager.find(CourseEntity.class, newCourse.getId());
        assertEquals(course.getTitle(), entity.getTitle());
    }
}