package ru.edu.project.backend.da.jdbcTemplate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.edu.project.backend.api.lessons.Lesson;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LessonDATest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private NamedParameterJdbcTemplate jdbcUpdate;

    @InjectMocks
    private LessonDA da;

    private Lesson expLesson = Lesson
            .builder()
            .title("Lesson")
            .courseId(10L)
            .description("Description")
            .homework("Homework")
            .build();

    @Test
    public void getLessons() throws SQLException {
        expLesson.setId(10L);
        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong("id")).thenReturn(expLesson.getId());
        when(rs.getString("lesson_title")).thenReturn(expLesson.getTitle());
        when(rs.getString("lesson_description")).thenReturn(expLesson.getDescription());
        when(rs.getString("lesson_homework")).thenReturn(expLesson.getHomework());
        when(rs.getLong("lesson_course_id")).thenReturn(expLesson.getCourseId());
        Answer<List<Lesson>> answer = new Answer<List<Lesson>>() {
            public List<Lesson> answer(InvocationOnMock invocationOnMock) throws Throwable {
                RowMapper<Lesson> mapper = invocationOnMock.getArgument(1);
                Lesson lesson = mapper.mapRow(rs, 1);
                return Arrays.asList(lesson);
            }
        };
        when(jdbcTemplate.query(any(String.class), any(RowMapper.class))).thenAnswer(answer);
        List<Lesson> lessons = da.getLessons();
        assertEquals(1, lessons.size());
        assertEquals(expLesson.getId(), lessons.get(0).getId());
        assertEquals(expLesson.getTitle(), lessons.get(0).getTitle());
        assertEquals(expLesson.getDescription(), lessons.get(0).getDescription());
        assertEquals(expLesson.getHomework(), lessons.get(0).getHomework());
        assertEquals(expLesson.getCourseId(), lessons.get(0).getCourseId());
    }

    @Test
    public void getById() throws SQLException {
        expLesson.setId(10L);
        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong("id")).thenReturn(10L);
        when(rs.getString("lesson_title")).thenReturn(expLesson.getTitle());
        when(rs.getString("lesson_description")).thenReturn(expLesson.getDescription());
        when(rs.getString("lesson_homework")).thenReturn(expLesson.getHomework());
        when(rs.getLong("lesson_course_id")).thenReturn(expLesson.getCourseId());
        Answer<Lesson> answer = new Answer<Lesson>() {
            public Lesson answer(InvocationOnMock invocationOnMock) throws Throwable {
                ResultSetExtractor<Lesson> extractor = invocationOnMock.getArgument(1);
                Lesson lesson = extractor.extractData(rs);
                return lesson;
            }
        };
        when(jdbcTemplate.query(any(String.class), any(ResultSetExtractor.class), any(Long.class))).thenAnswer(answer);
        Lesson actualLesson = da.getById(10L);
        assertEquals(expLesson.getId(), actualLesson.getId());
        assertEquals(expLesson.getTitle(), actualLesson.getTitle());
        assertEquals(expLesson.getHomework(), actualLesson.getHomework());
        assertEquals(expLesson.getDescription(), actualLesson.getDescription());
        assertEquals(expLesson.getCourseId(), actualLesson.getCourseId());
    }

    @Test
    public void update() {
        expLesson.setId(10L);
        SimpleJdbcInsert jdbcInsert = mock(SimpleJdbcInsert.class);
        when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        da.setJdbcInsert(jdbcInsert);
        Lesson lesson = da.save(expLesson);
        assertEquals(10, lesson.getId().longValue());
    }

    @Test
    public void insert() {
        SimpleJdbcInsert jdbcInsert = mock(SimpleJdbcInsert.class);
        when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        when( jdbcInsert.executeAndReturnKey(any(Map.class))).thenReturn(22L);
        da.setJdbcInsert(jdbcInsert);

        Lesson lesson = da.save(expLesson);
        assertEquals(22, lesson.getId().longValue());
    }
}