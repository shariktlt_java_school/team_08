package ru.edu.project.backend.da.jpa.converter;

import org.junit.Test;
import org.mapstruct.factory.Mappers;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.da.jpa.entity.CourseEntity;
import ru.edu.project.backend.da.jpa.entity.LessonEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class LessonMapperTest {
    private LessonMapper mapper = Mappers.getMapper(LessonMapper.class);
    private Lesson lesson = Lesson
            .builder()
            .id(10L)
            .title("Lesson title")
            .description("description")
            .homework("Homework")
            .courseId(25L)
            .build();

    @Test
    public void mapToEntity() {
        LessonEntity entity = mapper.map(lesson);
        assertEquals(entity.getTitle(), lesson.getTitle());
        assertEquals(entity.getId(), lesson.getId());
        assertEquals(entity.getDescription(), lesson.getDescription());
        assertEquals(entity.getHomework(), lesson.getHomework());
        assertEquals(entity.getCourseId(), lesson.getCourseId());
    }

    @Test
    public void mapToEntityNull() {
        Lesson nullLesson = null;
        LessonEntity entity = mapper.map(nullLesson);
        assertNull(entity);
    }
    
    @Test
    public void testToLesson() {
        LessonEntity entity = mapper.map(lesson);
        Lesson newLesson = mapper.map(entity);
        assertEquals(newLesson.getTitle(), entity.getTitle());
    }


    @Test
    public void mapToLessonNull() {
        LessonEntity nullLessonEntity = null;
        Lesson lesson = mapper.map(nullLessonEntity);
        assertNull(lesson);
    }
    
    @Test
    public void mapList() {
        LessonEntity entity = mapper.map(lesson);
        List<LessonEntity> entities = Arrays.asList(entity);
        List<Lesson> lessons = mapper.mapList(entities);
        assertEquals(1, lessons.size());
        assertEquals(entity.getTitle(), lessons.get(0).getTitle());
    }

    @Test
    public void mapListNull() {
        List<LessonEntity> entities = null;
        List<Lesson> lessons = mapper.mapList(entities);
        assertNull(lessons);
    }

}