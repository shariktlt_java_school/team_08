package ru.edu.project.backend.da.jpa.converter;

import org.junit.Test;
import org.mapstruct.factory.Mappers;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.da.jpa.entity.GradeEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GradeMapperTest {

    private GradeMapper mapper = Mappers.getMapper(GradeMapper.class);
    private Grade grade = Grade
            .builder()
            .id(1L)
            .lessonId("2")
            .groupStudentId("3")
            .grade("5")
            .build();

    @Test
    public void mapToEntity() {
        GradeEntity entity = mapper.map(grade);
        assertEquals(entity.getId(), grade.getId());
        assertEquals(entity.getLessonId(), grade.getLessonId());
        assertEquals(entity.getGroupStudentId(), grade.getGroupStudentId());
        assertEquals(entity.getGrade(), grade.getGrade());
    }

    @Test
    public void testToCourse() {
        GradeEntity entity = mapper.map(grade);
        Grade newGrade = mapper.map(entity);
        assertEquals(newGrade.getGrade(), entity.getGrade());
    }

    @Test
    public void mapList() {
        GradeEntity entity = mapper.map(grade);
        List<GradeEntity> entities = Arrays.asList(entity);
        List<Grade> grades = mapper.mapList(entities);
        assertEquals(1, grades.size());
        assertEquals(entity.getGrade(), grades.get(0).getGrade());
    }
}