package ru.edu.project.backend.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.da.GroupDALayer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceLayerTest {

    public static final long GROUP_ID = 1L;
    public static final long GROUP_COURSE_ID = 1L;
    public static final String GROUP_TITLE = "TU-1";

    @Mock
    private GroupDALayer mockDALayer;

    @InjectMocks
    private GroupServiceLayer serviceLayer;

    private Group group =
            Group.builder()
                    .id(GROUP_ID)
                    .courseId(GROUP_COURSE_ID)
                    .title(GROUP_TITLE)
                    .started(true)
                    .build();

    @Test
    public void createGroup() {

        when(mockDALayer.save(any(Group.class))).thenReturn(group);

        GroupForm form = GroupForm.builder().build();
        Group newGroup = serviceLayer.createGroup(form);

        assertEquals(group.getCourseId(), newGroup.getCourseId());
    }

    @Test
    public void getGroups() {

        when(mockDALayer.getGroups()).thenReturn(Arrays.asList(group));

        List<Group> groups = serviceLayer.getGroups();
        assertEquals(1, groups.size());
        assertEquals(group.getCourseId(), groups.get(0).getCourseId());
    }

    @Test
    public void getGroupsByCourse() {

        when(mockDALayer.getGroupsByCourse(any(Long.class))).thenReturn(Arrays.asList(group));

        List<Group> groups = serviceLayer.getByCourse(GROUP_COURSE_ID);
        assertEquals(1, groups.size());
        assertEquals(group.getCourseId(), groups.get(0).getCourseId());
    }


    @Test
    public void updateGroup() {

        when(mockDALayer.save(any(Group.class))).thenReturn(group);

        GroupForm form = GroupForm.builder().build();
        Group updatedGroup = serviceLayer.updateGroup(GROUP_ID, form);

        assertEquals(group.getCourseId(), updatedGroup.getCourseId());
    }

    @Test
    public void getGroupById() {

        when(mockDALayer.getById(any(Long.class))).thenReturn(group);

        Group group1 = serviceLayer.getGroup(GROUP_ID);
        assertEquals(group.getCourseId(), group1.getCourseId());

    }
}
