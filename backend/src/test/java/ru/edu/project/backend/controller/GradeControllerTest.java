package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.api.grades.GradeForm;
import ru.edu.project.backend.api.grades.GradeService;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(GradeController.class)
public class GradeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private GradeService gradeService;

    @InjectMocks
    private GradeController controller;

    private Grade mockGrade = Grade
            .builder()
            .id(1L)
            .lessonId("2")
            .groupStudentId("3")
            .grade("5")
            .build();

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getGrades() throws Exception {
        when(gradeService.getGrades()).thenReturn(Arrays.asList(mockGrade));
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/grade/getGrades").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "[{\"id\":1,\"lessonId\":\"2\",\"groupStudentId\":\"3\",\"grade\":\"5\"}]";
        assertEquals(test, result.getResponse().getContentAsString());
    }

    @Test
    public void createCourse() throws Exception {
        when(gradeService.createGrade(any(GradeForm.class))).thenReturn(mockGrade);
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/grade/createGrade")
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        String test = "{\"id\":1,\"lessonId\":\"2\",\"groupStudentId\":\"3\",\"grade\":\"5\"}";
        assertEquals(test, result.getResponse().getContentAsString());
    }

    @Test
    public void saveGrade() throws Exception {
        when(gradeService.saveGrade(any(GradeForm.class))).thenReturn(mockGrade);
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/grade/saveGrade")
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "{\"id\":1,\"lessonId\":\"2\",\"groupStudentId\":\"3\",\"grade\":\"5\"}";
        String actual = result.getResponse().getContentAsString();
        assertEquals(test, actual);
    }

    @Test
    public void getGradeById() throws Exception {
        when(gradeService.getGrade(any(Long.class))).thenReturn(mockGrade);
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/grade/getGrade/1")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "{\"id\":1,\"lessonId\":\"2\",\"groupStudentId\":\"3\",\"grade\":\"5\"}";
        String actual = result.getResponse().getContentAsString();
        assertEquals(test, actual);
    }
}