package ru.edu.project.backend.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseForm;
import ru.edu.project.backend.da.CourseDALayer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceLayerTest {

    @Mock
    private CourseDALayer mockDALayer;

    @InjectMocks
    private CourseServiceLayer serviceLayer;

    private Course course = Course
            .builder()
            .id(1L)
            .title("Новый курс")
            .description("Описание")
            .teacherId(5L)
            .build();

    @Test
    public void createCourse() {
        when(mockDALayer.save(any(Course.class))).thenReturn(course);
        CourseForm form = CourseForm.builder().build();
        Course newCourse = serviceLayer.createCourse(form);
        assertEquals(course.getTitle(), newCourse.getTitle());
    }

    @Test
    public void getCourses() {
        when(mockDALayer.getCourses()).thenReturn(Arrays.asList(course));
        List<Course> courses = serviceLayer.getCourses();
        assertEquals(1, courses.size());
        assertEquals(course.getTitle(), courses.get(0).getTitle());
    }

    @Test
    public void saveCourse() {
        when(mockDALayer.save(any(Course.class))).thenReturn(course);
        CourseForm form = CourseForm.builder().build();
        Course updatedCourse = serviceLayer.saveCourse(form);
        assertEquals(course.getTitle(), updatedCourse.getTitle());
    }

    @Test
    public void getCourseById() {
        when(mockDALayer.getById(any(Long.class))).thenReturn(course);
        Course course1 = serviceLayer.getCourse(1L);
        assertEquals(course.getTitle(), course1.getTitle());
    }
}