package ru.edu.project.backend.da.jdbcTemplate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.edu.project.backend.api.grades.Grade;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GradeDATest {

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private NamedParameterJdbcTemplate jdbcUpdate;

    @InjectMocks
    private GradeDA da;

    private Grade mockGrade = Grade
            .builder()
            .id(1L)
            .lessonId("2")
            .groupStudentId("3")
            .grade("5")
            .build();

    @Test
    public void getGrades() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong("grade_id")).thenReturn(1L);
        when(rs.getString("grade_lesson_id")).thenReturn("2");
        when(rs.getString("grade_group_student_id")).thenReturn("3");
        when(rs.getString("grade")).thenReturn("5");
        Answer<List<Grade>> answer = new Answer<List<Grade>>() {
            public List<Grade> answer(InvocationOnMock invocationOnMock) throws Throwable {
                RowMapper<Grade> mapper = invocationOnMock.getArgument(1);
                Grade grade = mapper.mapRow(rs, 1);
                return Arrays.asList(grade);
            }
        };
        when(jdbcTemplate.query(any(String.class), any(RowMapper.class))).thenAnswer(answer);
        List<Grade> grades = da.getGrades();
        assertEquals(1, grades.size());
        assertEquals(mockGrade.getGrade(), grades.get(0).getGrade());
    }

    @Test
    public void getById() throws SQLException {
        ResultSet rs = mock(ResultSet.class);
        when(rs.getLong("grade_id")).thenReturn(1L);
        when(rs.getString("grade_lesson_id")).thenReturn("2");
        when(rs.getString("grade_group_student_id")).thenReturn("3");
        when(rs.getString("grade")).thenReturn("5");
        Answer<Grade> answer = new Answer<Grade>() {
            public Grade answer(InvocationOnMock invocationOnMock) throws Throwable {
                ResultSetExtractor<Grade> extractor = invocationOnMock.getArgument(1);
                Grade grade = extractor.extractData(rs);
                return grade;
            }
        };
        when(jdbcTemplate.query(any(String.class), any(ResultSetExtractor.class), any(Long.class))).thenAnswer(answer);
        Grade grade = da.getById(1L);
        assertEquals(mockGrade.getId(), grade.getId());
        assertEquals(mockGrade.getLessonId(), grade.getLessonId());
        assertEquals(mockGrade.getGroupStudentId(), grade.getGroupStudentId());
        assertEquals(mockGrade.getGrade(), grade.getGrade());
    }

    @Test
    public void save() {
        SimpleJdbcInsert jdbcInsert = mock(SimpleJdbcInsert.class);
        when(jdbcInsert.withTableName(any(String.class))).thenReturn(jdbcInsert);
        when(jdbcInsert.usingGeneratedKeyColumns(any(String.class))).thenReturn(jdbcInsert);
        da.setJdbcInsert(jdbcInsert);
        //when(jdbcInsert.executeAndReturnKey(any(Map.class))).thenReturn(1);
        Grade grade = da.save(mockGrade);
        assertEquals(1, grade.getId().longValue());

        when(jdbcUpdate.update(any(String.class), any(Map.class))).thenReturn(1);
        Grade newGrade = da.save(grade);
        assertEquals(1, newGrade.getId().longValue());
    }
}
