package ru.edu.project.backend.da.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.app.BackendApplication;
import ru.edu.project.backend.da.jpa.converter.UserMapper;
import ru.edu.project.backend.da.jpa.entity.UserEntity;

import java.util.List;

import static org.junit.Assert.*;

@ActiveProfiles("SPRING_DATA")
@RunWith(SpringRunner.class)
@DataJpaTest // автоматически создает конфигурцию для hibernate h2
@ContextConfiguration(classes = BackendApplication.class) // указывает где искать конфигурационный класс для спринг бут
public class JPAUserDATest {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private JPAUserDA repository;

    private UserMapper mapper = Mappers.getMapper(UserMapper.class);

   @Test
    public void getUsers() {
       String expected = "main";
       assertEquals(expected, "main");
        User user = User.builder().name("1").role(User.UserRole.Student).build();
        UserEntity newUser = manager.persist(mapper.map(user));
        User user2 = User.builder().name("2").role(User.UserRole.Administrator).build();
        UserEntity newUser2 = manager.persist(mapper.map(user2));
        List<User> foundUsers = repository.getUsers();
        assertEquals(2, foundUsers.size());
    }

    @Test
    public void getById() {
        User user = User.builder().name("1").role(User.UserRole.Student).build();
        UserEntity newUser = manager.persist(mapper.map(user));
        User foundUser = repository.getById(newUser.getId());
        assertNotNull(foundUser);
        assertEquals(user.getName(), foundUser.getName());
    }

    @Test
    public void save() {
        User user = User.builder().name("1").role(User.UserRole.Student).build();
        User newUser = repository.save(user);
        assertNotNull(newUser.getId());
        UserEntity entity = manager.find(UserEntity.class, newUser.getId());
        assertEquals(user.getName(), entity.getName());
    }

    @Test
    public void findByLogin() {
        User user = User.builder().name("1").role(User.UserRole.Student).login("1").build();
        UserEntity newUser = manager.persist(mapper.map(user));
        User foundUser = repository.getByLogin(user.getLogin());
        assertEquals(user.getName(), foundUser.getName());
    }
}