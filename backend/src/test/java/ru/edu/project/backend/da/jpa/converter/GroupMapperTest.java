package ru.edu.project.backend.da.jpa.converter;

import org.junit.Test;
import org.mapstruct.factory.Mappers;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GroupMapperTest {

    public static final long GROUP_ID = 1L;
    public static final long GROUP_COURSE_ID = 1L;
    public static final String GROUP_TITLE = "TU-1";

    private GroupMapper mapper = Mappers.getMapper(GroupMapper.class);

    private Group group =
            Group.builder()
                    .id(GROUP_ID)
                    .courseId(GROUP_COURSE_ID)
                    .title(GROUP_TITLE)
                    .started(true)
                    .build();

    @Test
    public void mapToEntity() {
        GroupEntity entity = mapper.map(group);
        assertEquals(entity.getId(), group.getId());
        assertEquals(entity.getCourseId(), group.getCourseId());
        assertEquals(entity.getTitle(), group.getTitle());
        assertEquals(entity.isStarted(), group.isStarted());
    }

    @Test
    public void testToCourse() {
        GroupEntity entity = mapper.map(group);
        Group newGroup = mapper.map(entity);
        assertEquals(newGroup.getId(), entity.getId());
        assertEquals(newGroup.getCourseId(), entity.getCourseId());
        assertEquals(newGroup.getTitle(), entity.getTitle());
        assertEquals(newGroup.isStarted(), entity.isStarted());
    }

    @Test
    public void mapList() {
        GroupEntity entity = mapper.map(group);
        List<GroupEntity> entities = Arrays.asList(entity);
        List<Group> groups = mapper.mapList(entities);

        assertEquals(1, groups.size());
        assertEquals(entity.getId(), groups.get(0).getId());
        assertEquals(entity.getCourseId(), groups.get(0).getCourseId());
        assertEquals(entity.getTitle(), groups.get(0).getTitle());
        assertEquals(entity.isStarted(), groups.get(0).isStarted());
    }
}