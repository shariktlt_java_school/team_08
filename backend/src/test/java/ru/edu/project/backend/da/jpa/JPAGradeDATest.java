package ru.edu.project.backend.da.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.app.BackendApplication;
import ru.edu.project.backend.da.jpa.converter.GradeMapper;
import ru.edu.project.backend.da.jpa.entity.GradeEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("SPRING_DATA")
@RunWith(SpringRunner.class)
@DataJpaTest // автоматически создает конфигурцию для hibernate h2
@ContextConfiguration(classes = BackendApplication.class) // указывает где искать конфигурационный класс для спринг бут
public class JPAGradeDATest {

    @Autowired
    private TestEntityManager manager;

    @Autowired
    private JPAGradeDA repository;

    private final GradeMapper mapper = Mappers.getMapper(GradeMapper.class);

    @Test
    public void getGrades() {
        System.out.println("IN GET GRADES JPA TEST");
        Grade grade1 = Grade
                .builder()
                .lessonId("2")
                .groupStudentId("3")
                .grade("5")
                .build();
        Grade grade2 = Grade
                .builder()
                .lessonId("3")
                .groupStudentId("3")
                .grade("3")
                .build();
        manager.persist(mapper.map(grade1));
        manager.persist(mapper.map(grade2));
        List<Grade> foundGrades = repository.getGrades();
        assertEquals(2, foundGrades.size());
    }

    @Test
    public void getById() {
        Grade grade = Grade
                .builder()
                .lessonId("4")
                .groupStudentId("1")
                .grade("4")
                .build();
        manager.persist(mapper.map(grade));
        Grade foundGrade = repository.getById(1L);
        assertNotNull(foundGrade);
        assertEquals(grade.getLessonId(), foundGrade.getLessonId());
    }

    @Test
    public void save() {
        Grade grade = Grade
                .builder()
                .id(1L)
                .lessonId("5")
                .groupStudentId("2")
                .grade("3")
                .build();
        Grade newGrade = repository.save(grade);
        assertNotNull(newGrade.getId());
        GradeEntity entity = manager.find(GradeEntity.class, newGrade.getId());
        assertEquals(grade.getLessonId(), entity.getLessonId());
    }
}
