package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseForm;
import ru.edu.project.backend.api.courses.CourseService;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(CourseController.class)
public class CourseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private CourseService courseService;

    @InjectMocks
    private CourseController controller;

    private Course mockCourse = Course
            .builder()
            .id(1L)
            .title("New course")
            .description("Description")
            .teacherId(5L)
            .build();

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getCourses() throws Exception {
        when(courseService.getCourses()).thenReturn(Arrays.asList(mockCourse));
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/course/getCourses").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "[{\"id\":1,\"title\":\"New course\",\"description\":\"Description\",\"teacherId\":5}]";
        assertEquals(test, result.getResponse().getContentAsString());
    }

    @Test
    public void createCourse() throws Exception {
        when(courseService.createCourse(any(CourseForm.class))).thenReturn(mockCourse);
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/course/createCourse")
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        String test = "{\"id\":1,\"title\":\"New course\",\"description\":\"Description\",\"teacherId\":5}";
        assertEquals(test, result.getResponse().getContentAsString());
    }

    @Test
    public void saveCourse() throws Exception {
        when(courseService.saveCourse(any(CourseForm.class))).thenReturn(mockCourse);
        String json = "{}";
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/course/saveCourse")
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "{\"id\":1,\"title\":\"New course\",\"description\":\"Description\",\"teacherId\":5}";
        String actual = result.getResponse().getContentAsString();
        assertEquals(test, actual);
    }

    @Test
    public void getCourseById() throws Exception {
        when(courseService.getCourse(any(Long.class))).thenReturn(mockCourse);
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/course/getCourse/1")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();
        String test = "{\"id\":1,\"title\":\"New course\",\"description\":\"Description\",\"teacherId\":5}";
        String actual = result.getResponse().getContentAsString();
        assertEquals(test, actual);
    }
}