package ru.edu.project.backend.da.jpa.converter;

import org.junit.Test;
import org.mapstruct.factory.Mappers;
import ru.edu.project.backend.api.groups.GroupStudent;
import ru.edu.project.backend.da.jpa.entity.GroupStudentEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GroupStudentMapperTest {

    public static final long ID = 1L;
    public static final long GROUP_ID = 1L;
    public static final long USER_ID = 1L;

    private GroupStudentMapper mapper = Mappers.getMapper(GroupStudentMapper.class);

    private GroupStudent groupStudent =
            GroupStudent
                    .builder()
                    .id(ID)
                    .groupId(GROUP_ID)
                    .userId(USER_ID)
                    .build();

    @Test
    public void mapToEntity() {
        GroupStudentEntity entity = mapper.map(groupStudent);
        assertEquals(entity.getId(), groupStudent.getId());
        assertEquals(entity.getGroupId(), groupStudent.getGroupId());
        assertEquals(entity.getUserId(), groupStudent.getUserId());
    }

    @Test
    public void testToCourse() {
        GroupStudentEntity entity = mapper.map(groupStudent);
        GroupStudent newGroupStudent = mapper.map(entity);
        assertEquals(newGroupStudent.getId(), entity.getId());
        assertEquals(newGroupStudent.getGroupId(), entity.getGroupId());
        assertEquals(newGroupStudent.getUserId(), entity.getUserId());
    }

    @Test
    public void mapList() {
        GroupStudentEntity entity = mapper.map(groupStudent);
        List<GroupStudentEntity> entities = Arrays.asList(entity);
        List<GroupStudent> groupStudents = mapper.mapList(entities);

        assertEquals(1, groupStudents.size());
        assertEquals(entity.getId(), groupStudents.get(0).getId());
        assertEquals(entity.getGroupId(), groupStudents.get(0).getGroupId());
        assertEquals(entity.getUserId(), groupStudents.get(0).getUserId());
    }
}