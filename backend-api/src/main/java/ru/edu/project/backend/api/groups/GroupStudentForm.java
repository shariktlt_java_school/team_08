package ru.edu.project.backend.api.groups;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Jacksonized
@Builder
public class GroupStudentForm {

    /**
     * Group's id.
     */
    private Long groupId;

    /**
     * Student's user_id.
     */
    private Long userId;

}
