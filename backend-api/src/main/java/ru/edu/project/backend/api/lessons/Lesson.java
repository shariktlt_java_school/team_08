package ru.edu.project.backend.api.lessons;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
@ToString
public class Lesson {

    /**
     * Идентификатор урока.
     */
    private Long id;

    /**
     * Идентификатор связанного курса.
     */
    private Long courseId;

    /**
     * Наименование урока.
     */
    private String title;

    /**
     * Описание урока.
     */
    private String description;

    /**
     * Домашнее задание на урок.
     */
    private String homework;
}
