package ru.edu.project.backend.api.students;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
@ToString
@EqualsAndHashCode
public class User implements UserAbstract {
    @Jacksonized
    public enum UserRole {

        /**
         * Administrator role.
         */
        Administrator,

        /**
         * Teacher role.
         */
        Teacher,

        /**
         * Student role.
         */
        Student
    }

    /**
     * id.
     */
    private Long id;
    /**
     * Имя.
     */
    private String name;
    /**
     * Фамилия.
     */
    private String surname;
    /**
     * Отчество.
     */
    private String middleName;
    /**
     * роль.
     */
    private UserRole role;
    /**
     * логин.
     */
    private String login;
    /**
     * пароль.
     */
    private String password;
}
