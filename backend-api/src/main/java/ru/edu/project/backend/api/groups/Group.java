package ru.edu.project.backend.api.groups;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
@ToString
@EqualsAndHashCode
public class Group {

    /**
     * Group's id.
     */
    private Long id;

    /**
     * Group's course id.
     */
    private Long courseId;

    /**
     * Group's title.
     */
    private String title;

    /**
     * Is group started their education.
     */
    private boolean started;

}
