package ru.edu.project.backend.api.courses;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.lessons.Lesson;
import ru.edu.project.backend.api.students.User;

import java.util.List;

@Getter
@Setter
@Builder
@Jacksonized
@ToString
@EqualsAndHashCode
public class CourseInfo {

    /**
     * Курс.
     */
    private Course course;

    /**
     * Привязанный к курсу преподаватель.
     */
    private User teacher;

    /**
     * Привязанные к курсу группы.
     */
    private List<Group> groups;

    /**
     * Уроки курса.
     */
    private List<Lesson> lessons;
}
