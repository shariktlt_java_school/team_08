package ru.edu.project.backend.api.grades;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Jacksonized
@Builder
public class GradeForm {

    /**
     * Идентификатор оценки.
     */
    private Long id;

    /**
     * lesson id.
     */
    private String lessonId;

    /**
     * groupStudent id.
     */
    private String groupStudentId;

    /**
     * grade.
     */
    private String grade;

}
