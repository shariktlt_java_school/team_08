package ru.edu.project.backend.api.students;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface UserService {
    /**
     * создание студента через .builder.
     * @param form
     * @return new student
     */
    @AcceptorArgument
    User createUser(UserForm form);

    /**
     *
     * @return list
     */
    List<User> getUsers();

    /**
     *
     * @param id
     * @param form
     * @return updated
     */
    User updateUser(Long id, UserForm form);

    /**
     *
     * @param login
     * @return user by login
     */
    User getUserByLogin(String login);
}
