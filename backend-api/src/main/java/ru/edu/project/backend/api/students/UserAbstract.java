package ru.edu.project.backend.api.students;

public interface UserAbstract {
    /**
     * ID.
     * @return String
     */
    Long getId();

    /**
     * Name.
     * @return String
     */
    String getName();

    /**
     * Surname.
     * @return String
     */
    String getSurname();

    /**
     * Middle name.
     * @return String
     */
    String getMiddleName();

    /**
     * Role.
     * @return UserRole
     */
    User.UserRole getRole();

    /**
     * Login.
     * @return String
     */
    String getLogin();

    /**
     * Password.
     * @return String
     */
    String getPassword();
}
