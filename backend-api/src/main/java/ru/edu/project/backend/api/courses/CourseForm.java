package ru.edu.project.backend.api.courses;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Jacksonized
@Builder
public class CourseForm {

    /**
     * Идентификатор курса.
     */
    private Long id;

    /**
     * Наименование курса.
     */
    private String title;

    /**
     * Описание курса.
     */
    private String description;

    /**
     * id преподавателя.
     */
    private Long teacherId;

}
