package ru.edu.project.backend.api.lessons;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Jacksonized
@Builder
public class LessonForm {

    /**
     * Идентификатор урока.
     */
    private Long id;

    /**
     * Идентификатор связанного курса.
     */
    private Long courseId;

    /**
     * Наименование урока.
     */
    private String title;

    /**
     * Описание урока.
     */
    private String description;

    /**
     * Домашнее задание на урок.
     */
    private String homework;
}
