package ru.edu.project.backend.api.groups;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface GroupStudentService {

    /**
     * Creating info for Student in Group.
     * @param form Data from Group's student form
     * @return new GroupStudent object
     */
    @AcceptorArgument
    GroupStudent addStudentToGroup(GroupStudentForm form);

    /**
     * Get all GroupStudent objects.
     * @return list of GroupStudent objects
     */
    List<GroupStudent> getGroupStudents();

    /**
     * Get all GroupStudent objects by Group id.
     *
     * @param groupId Group id
     * @return list of GroupStudent objects
     */
    List<GroupStudent> getByGroup(Long groupId);

    /**
     * Get GroupStudent objects by id.
     *
     * @param id Group id
     * @return list of GroupStudent objects
     */
    GroupStudent getGroupStudent(Long id);

    /**
     * Delete Student from Group's students list.
     *
     * @param groupId Group id
     * @param userId  User id
     * @return updated Group object
     */
    @AcceptorArgument
    boolean deleteStudentFromGroup(Long groupId, Long userId);

//    /**
//     * Get id for Group's Student's row in database.
//     * @param groupId Group id
//     * @param userId User id
//     * @return id for Group's Student's row in database
//     */
//    String getIdStudentFromGroup(String groupId, String userId);
//
//    /**
//     * Get Group's Student's row in database.
//     * @param groupId Group id
//     * @param userId User id
//     * @return id for Group's Student's row in database
//     */
//    GroupStudent getStudentFromGroup(String groupId, String userId);

}
