package ru.edu.project.backend.api.groups;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class GroupStudent {

    /**
     * Student's id in his group.
     */
    private Long id;

    /**
     * Group's id.
     */
    private Long groupId;

    /**
     * Student's user_id.
     */
    private Long userId;

}
