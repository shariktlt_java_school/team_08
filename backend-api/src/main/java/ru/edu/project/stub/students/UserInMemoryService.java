package ru.edu.project.stub.students;

import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserForm;
import ru.edu.project.backend.api.students.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class UserInMemoryService implements UserService {

    /**
     * Счетчик для поля id.
     */
    private Long userIdCounter = 0L;

    /**
     * map of all students.
     **/
    private ConcurrentHashMap<Long, User> users = new ConcurrentHashMap<>();

    /**
     * Конструктор.
     */
    public UserInMemoryService() {
        User student = User.builder()
                .role(User.UserRole.Student)
                .name("Иван")
                .middleName("Иванович")
                .surname("Иванов")
                .login("Иван")
                .id(userIdCounter++)
                .build();
        users.put(student.getId(), student);

        student = User.builder()
                .role(User.UserRole.Student)
                .name("Петр")
                .middleName("Петрович")
                .surname("Петров")
                .login("Петр")
                .id(userIdCounter++)
                .build();
        users.put(student.getId(), student);

        student = User.builder()
                .role(User.UserRole.Student)
                .name("Василий")
                .middleName("Алибабаевич")
                .surname("Васильев")
                .login("Василий")
                .id(userIdCounter++)
                .build();
        users.put(student.getId(), student);

        User teacher = User.builder()
                .role(User.UserRole.Teacher)
                .name("Филипп")
                .middleName("Филиппович")
                .surname("Филипов")
                .login("Филипп")
                .id(userIdCounter++)
                .build();
        users.put(teacher.getId(), teacher);

        teacher = User.builder()
                .role(User.UserRole.Teacher)
                .name("Алексей")
                .middleName("")
                .surname("Учитель")
                .login("Алексей")
                .id(userIdCounter++)
                .build();
        users.put(teacher.getId(), teacher);

        teacher = User.builder()
                .role(User.UserRole.Teacher)
                .name("Ипполи́т")
                .middleName("Матве́евич")
                .surname("Воробья́нинов")
                .login("Ипполит")
                .id(userIdCounter++)
                .build();
        users.put(teacher.getId(), teacher);
    }

    /**
     * @param form
     * @return new student
     */
    @Override
    public User createUser(final UserForm form) {
        if (form == null) {
            throw new IllegalArgumentException("form is null");
        }
        if (form.getRole() == null) {
            throw new
                    IllegalArgumentException("Cannot create user without role");
        }
        User result = User.builder().
                id(userIdCounter++).
                name(form.getName()).
                surname(form.getSurname()).
                middleName(form.getMiddleName()).
                login(form.getLogin()).
                role(form.getRole()).
                build();
        users.put(result.getId(), result);
        return result;
    }

    /**
     * @return list
     */
    @Override
    public List<User> getUsers() {
        return new ArrayList<>(users.values());
    }

    /**
     * @param id
     * @return updated
     */
    @Override
    public User updateUser(final Long id, final UserForm form) {
        User st = users.get(id);
        if (st == null) {
            throw new IllegalArgumentException("id not found");
        }
        if (form.getName() != null) {
            st.setName(form.getName());
        }
        if (form.getSurname() != null) {
            st.setSurname(form.getSurname());
        }
        if (form.getMiddleName() != null) {
            st.setMiddleName(form.getMiddleName());
        }
        users.put(st.getId(), st);
        return st;
    }

    /**
     *
     * @param login
     * @return user
     */
    @Override
    public User getUserByLogin(final String login) {
        User result = null;
        for (User user : users.values()) {
            if (user.getLogin().equals(login)) {
                result = user;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("No user");
        }
        return result;
    }
}
