package ru.edu.project.stub.grades;

import ru.edu.project.backend.api.grades.Grade;
import ru.edu.project.backend.api.grades.GradeForm;
import ru.edu.project.backend.api.grades.GradeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GradeInMemoryService implements GradeService {
    /**
     * storage.
     */
    private ConcurrentHashMap<Long, Grade> grades =
            new ConcurrentHashMap<>();

    /**
     * Счетчик для идентификаторов курсов.
     */
    private Long gradeId = 0L;

    /**
     * create Grade.
     *
     * @param form
     * @return new Grade.
     */
    @Override
    public Grade createGrade(final GradeForm form) {
        if (form == null || form.getGrade() == null) {
            throw new IllegalArgumentException("invalid form");
        }
        // Проверяет, существует ли оценка для связки LessonId - StudentId
        // если нет, то вернет IllegalArgumentException!
        for (Map.Entry<Long, Grade> item : grades.entrySet()) {
            if (item.getValue()
                    .getLessonId()
                    .equals(form.getLessonId()) && item.getValue()
                    .getGroupStudentId()
                    .equals(form.getGroupStudentId())) {
                throw new IllegalArgumentException("Already have this Grade!");
            }
        }
        // идентификаторы урока и groupUser приходят из формы
        // String lessonId = "1"; // условный идентификатор урока
        // String groupUserId = "1"; // условный идентификатор groupUser
        //Long id = Long.parseLong(UUID.randomUUID().toString());
        Long id = gradeId++;
        Grade newGrade = Grade.builder()
                .id(id)
                .grade(form.getGrade())
                .lessonId(form.getLessonId())
                .groupStudentId(form.getGroupStudentId())
                .build();
        grades.put(id, newGrade);
        return newGrade;
    }

    /**
     * get all Grades.
     *
     * @return list.
     */
    @Override
    public List<Grade> getGrades() {
        return new ArrayList<>(grades.values());
    }

    /*
    public Grade saveGrade(final String id, final GradeForm form) {
        Grade grade = grades.get(id);
        if (grade == null) {
            throw new IllegalArgumentException("grade id not found");
        }
        if (form.getLessonId() != null) {
            grade.setLessonId(form.getLessonId());
        }
        if (form.getGroupStudentId() != null) {
            grade.setGroupStudentId(form.getGroupStudentId());
        }
        grades.put(grade.getId(), grade);
        return grade;
    }
     */

    /**
     * Сохранение существующей оценки после редактирования.
     *
     * @param form - GradeForm
     * @return Grade.
     */
    @Override
    public Grade saveGrade(final GradeForm form) {
//        System.out.println("____________________IN MEMORY");
//        System.out.println("____________________IN MEMORY_form_id: " + form.getId());
//        System.out.println("____________________IN MEMORY_form_lesson_id: " + form.getLessonId());
//        System.out.println("____________________IN MEMORY_form_groupst_id: " + form.getGroupStudentId());
//        System.out.println("____________________IN MEMORY_form_grade: " + form.getGrade());
        Grade grade = null;
        if (grades.containsKey(form.getId())) {
            grade = Grade
                    .builder()
                    .id(form.getId())
                    .lessonId(form.getLessonId())
                    .groupStudentId(form.getGroupStudentId())
                    .grade(form.getGrade())
                    .build();
            grades.put(form.getId(), grade);
        }
        return grade;
    }

    /**
     * Get Grade by id.
     *
     * @param id Grade id
     * @return Grade object
     */
    @Override
    public Grade getGrade(final Long id) {
        return grades.get(id);
    }

    /**
     * Delete Grade by id.
     *
     * @param id Grade id
     * @return Grade was deleted?
     */
    @Override
    public Boolean deleteGrade(final Long id) {
        System.out.println("____IN_DELETE_MEMORY_SERVICE");
        if (grades.get(id) != null) {
            grades.remove(id);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete All Grades.
     *
     * @return All grades were deleted?
     */
    @Override
    public Boolean deleteGrades() {
        System.out.println("____________________IN MEMORY__DELETE_ALL_GRADES");
        if (grades.size() > 0) {
            grades = new ConcurrentHashMap<>();
            return true;
        } else {
            return false;
        }
    }
}
