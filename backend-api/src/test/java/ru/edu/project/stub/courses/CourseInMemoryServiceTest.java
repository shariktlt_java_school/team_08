package ru.edu.project.stub.courses;

import org.junit.Test;
import ru.edu.project.backend.api.courses.Course;
import ru.edu.project.backend.api.courses.CourseForm;
import ru.edu.project.backend.api.courses.CourseService;
import ru.edu.project.stub.courses.CourseInMemoryService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CourseInMemoryServiceTest {
    CourseService service = new CourseInMemoryService();
    Course course = service.createCourse(CourseForm.builder().title("new Course").build());

    @Test
    public void createCourse() {
        assertEquals("new Course", course.getTitle());
        assertNotNull(course.getId());
    }

    @Test
    public void getCourse() {
        List<Course> courses = service.getCourses();
        assertEquals(1, courses.size());
        assertEquals("new Course", courses.get(0).getTitle());
        assertEquals(course.toString(), courses.get(0).toString());
    }
}