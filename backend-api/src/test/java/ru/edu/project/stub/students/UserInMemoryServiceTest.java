package ru.edu.project.stub.students;

import org.junit.Test;
import ru.edu.project.backend.api.students.User;
import ru.edu.project.backend.api.students.UserForm;
import ru.edu.project.backend.api.students.UserService;

import java.util.List;

import static org.junit.Assert.*;

public class UserInMemoryServiceTest {
    UserService service = new UserInMemoryService();
    User st = service.createUser(UserForm.
            builder().
            name("Vasya").
            surname("Petrov").
            middleName("Vasilevich").
            login("Vasya").
            role(User.UserRole.Student).
            build());
    @Test
    public void createUser() {
        assertNotNull(st.getId());
        assertEquals(st.getName(), "Vasya");
    }

    @Test
    public void getUsers() {
        List<User> users = service.getUsers();
        assertEquals(7, users.size());
    }

    @Test
    public void updateUser() {
        User updated = service.updateUser(st.getId(), UserForm.builder().name("Boris").build());
        assertEquals(st.getId(), updated.getId());
        assertEquals("Boris", updated.getName());
        assertEquals(st.getSurname(), updated.getSurname());
    }

    @Test
    public void getUserByLogin() {
        User user = service.getUserByLogin("Vasya");
        assertEquals(st.getId(), user.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNotExistedUserByLogin() {
        User user = service.getUserByLogin("Petr");
    }
}