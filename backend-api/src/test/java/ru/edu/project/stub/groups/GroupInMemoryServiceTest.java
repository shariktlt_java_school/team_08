package ru.edu.project.stub.groups;

import org.junit.Test;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;

import java.util.List;

import static org.junit.Assert.*;

public class GroupInMemoryServiceTest {

    public static final Long COURSE_ID = 1L;
    public static final Long NEW_COURSE_ID = 2L;
    public static final String TITLE = "АТ-1";
    public static final String NEW_TITLE = "УМ-1";
    public static final boolean IS_STARTED = false;
    public static final boolean NEW_IS_STARTED = true;

    GroupService service = new GroupInMemoryService();
    GroupForm groupForm = GroupForm
            .builder()
            .courseId(COURSE_ID)
            .title(TITLE)
            .started(IS_STARTED)
            .build();
    Group group = service.createGroup(groupForm);

    @Test
    public void classGroupTest() {
        group.setId(1L);
        group.setTitle(NEW_TITLE);
        group.setCourseId(NEW_COURSE_ID);
        group.setStarted(NEW_IS_STARTED);

        assertEquals(NEW_COURSE_ID, group.getCourseId());
        assertEquals(NEW_TITLE, group.getTitle());
        assertEquals(NEW_IS_STARTED, group.isStarted());

        System.out.println(group);
//        System.out.println(group.getId());
//        System.out.println(group.getTitle());
//        System.out.println(group.getCourseId());
//        System.out.println(group.isStarted());

        System.out.println(groupForm);
//        System.out.println(groupForm.getTitle());
//        System.out.println(groupForm.getCourseId());
//        System.out.println(groupForm.getIsStarted());
    }


    @Test
    public void createGroup() {
        assertEquals(COURSE_ID, group.getCourseId());
        assertEquals(TITLE, group.getTitle());
        assertEquals(IS_STARTED, group.isStarted());
        assertNotNull(group.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createGroupWithException() {
        service.createGroup(null);
    }

    @Test
    public void getGroups() {
        List<Group> groups;
        groups = service.getGroups();
        assertEquals(1, groups.size());
    }

    @Test
    public void getByCourse() {
        List<Group> groups;
        groups = service.getByCourse(COURSE_ID);
        assertEquals(1, groups.size());
    }

    @Test
    public void updateGroup() {

        Group updatedGroup =
            service.updateGroup(group.getId(),
                    GroupForm
                            .builder()
                            .courseId(NEW_COURSE_ID)
                            .title(NEW_TITLE)
                            .started(NEW_IS_STARTED)
                            .build());
        assertEquals(NEW_COURSE_ID, updatedGroup.getCourseId());
        assertEquals(NEW_TITLE, updatedGroup.getTitle());
        assertEquals(NEW_IS_STARTED, updatedGroup.isStarted());

    }

    @Test(expected = IllegalArgumentException.class)
    public void updateGroupWithException() {
        service.updateGroup(999L, null);
    }

    @Test
    public void getGroup() {
        Group newGroup = service.getGroup(group.getId());
        assertEquals(COURSE_ID, newGroup.getCourseId());
        assertEquals(TITLE, newGroup.getTitle());
        assertEquals(IS_STARTED, newGroup.isStarted());
    }

    @Test
    public void deleteGroup() {
        assertTrue(service.deleteGroup(group.getId()));
        assertFalse(service.deleteGroup(group.getId()));

    }

}
